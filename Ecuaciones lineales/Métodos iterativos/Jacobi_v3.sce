clc
clear
// Método de Jacobi
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Jacobi.

function x = jacobi(A,b,x0,eps)
    n = size(A,1);
    x = x0;
    xk = x;
    c = 0;
    suma = 0;
    
    for i = 1:n
        if i == 1 then
            suma = A(1,2:n)*xk(2:n);
        elseif i == n then
            suma = A(n,1:n-1)*xk(1:n-1);
        else
            suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n);
        end;
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    
    while abs(norm(x-xk)) > eps
        xk = x;
        
        for i = 1:n
            if i == 1 then
                suma = A(1,2:n)*xk(2:n);
            elseif i == n then
                suma = A(n,1:n-1)*xk(1:n-1);
            else
                suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n);
            end;
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        c = c+1;
    end
    disp(c)
endfunction
   
A = [1 -1 0; -1 2 -1; 0 -1 1.1]
b = [0 1 0]'
