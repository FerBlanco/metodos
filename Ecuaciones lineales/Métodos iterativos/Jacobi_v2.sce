clc
clear

// Método de Jacobi
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Jacobi.

function x = jacobi(A,b,x0,eps)
    n = size(A,1);
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(A(k:n,k)));
        pivot = k-1+i;
        temp = A(pivot,:); A(pivot,:) = A(k,:); A(k,:) = temp;
        temp = b(pivot,:); b(pivot,:) = b(k,:); b(k,:) = temp;
    end;
    
    // Control 
    N = diag(diag(A)); // Jacobi usa N = diagonal(A)
    
    for i = 1:n
        invN(i) = 1/N(i,i); // diagonal de N^(-1)
    end
    invN = diag(invN);
    
    cond = eye(n,n)-invN*A;
    norma = [norm(cond), norm(cond,1), norm(cond,'inf'), norm(cond,'fro')];
    
    // Chequeo la norma
    nmin = min(norma);
    disp(nmin,"La norma minima fue ");
    
    if nmin >= 1 then
        // Chequeo el radio espectral
        if max(abs(spec(cond))) >= 1 then
            disp("La solución no converge para todo valor inicial");
            x = %nan;
            return;
        end;
    end
    
    xk = x0;
    c = 0;
    
    // Primera iteración
    suma = A(1,2:n)*xk(2:n)
    x(1) = 1/A(1,1)*(b(1)-suma)
    
    for i = 2:n-1
        suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n)
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    
    suma = A(n,1:n-1)*xk(1:n-1)
    x(n) = 1/A(n,n)*(b(n)-suma)
    c = c+1;

    while abs(norm(x-xk)) > eps
        xk = x;
        suma = A(1,2:n)*xk(2:n)
        x(1) = 1/A(1,1)*(b(1)-suma)
    
        for i = 2:n-1
            suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n)
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        
        suma = A(n,1:n-1)*xk(1:n-1)
        x(n) = 1/A(n,n)*(b(n)-suma)
        c = c+1;
    end
    disp(c,"La cantidad de iteraciones fue ")
endfunction
   
A = [9 -2 0; -2 4 -1; 0 -1 1]
b = [5 1 0]'

x = jacobi(A,b,zeros(3,1),0.00001)
// x = 0.7391287
//     0.8260829
//     0.8260791

Ad = [1 0 -1; 2 2 3; 1 1 1]
bd = [2 4 1]'
x = jacobi(Ad,bd,zeros(3,1),0.00001)
// La solución no converge para todo valor inicial
