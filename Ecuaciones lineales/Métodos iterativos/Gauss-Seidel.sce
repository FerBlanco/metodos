// Método de gauss
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de gauss.

function x = gauss(A,b,x0,eps) // Versión de Rabaseda
    n = size(A,1);
    x = x0;
    xk = x;
    suma = 0;
    c = 0;
    
    for i = 1:n
        suma = 0;
        for j = 1:n
            if i <> j then
                suma = suma + A(i,j)*x(j);
            end;
        end;
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    c = c+1;
    
    while abs(norm(x-xk)) > eps
        xk = x;
        for i = 1:n
            suma = 0;
            for j = 1:n
                if i <> j then
                    suma = suma + A(i,j)*x(j);
                end;
            end;
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        c = c+1;
    end
    disp(c)
endfunction

A = [1 -1 0; -1 2 -1; 0 -1 1.1]
b = [0 1 0]'
