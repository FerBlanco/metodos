clc
clear

// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana sin pivoteo.

function [x,a] = gausselim(A,b)
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    
    a = [A b]; // Matriz aumentada
    n = nA;
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik=a(i,k)/a(k,k);
            a(i,k+1:n+1) = a(i,k+1:n+1)-a(k,k+1:n+1)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1:n)*x(i+1:n))/a(i,i)
    end;
endfunction

// Casos de prueba
A = [2 -3 4; 2 2 2; -2 -7 1]
b = [3 2 3]'
[x,a] = gausselim(A,b)
disp("Casos de prueba de eliminación de Gauss")
disp(x)
//  -4.4
//   1.4
//    4.

disp(a)
//   2.  -3.   4.   3.
//   0.   5.  -2.  -1.
//   0.   0.   1.   4.
