clc
clear

// Esta función obtiene la factorización A = LU
// dada la matriz A la función utiliza el metodo de Doolittle
// para obtener L y U
function [L,U]=doolittle(A)
    [n,m] = size(A);
    
    if n<>m then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    U = zeros(n,m);
    L = eye(n,m);
    
    U(1,:) = A(1,:); // Primera fila de U
    L(:,1) = A(:,1)/U(1,1); // Primera columna de L
    
    // Doolittle
    for i=2:n
        U(i,i:n) = A(i,i:n)-L(i,1:i-1)*U(1:i-1,i:n);
        L(i+1:n,i) = (A(i+1:n,i)-L(i+1:n,1:i-1)*U(1:i-1,i))/U(i,i);
    end;
endfunction

// Caso de prueba 
A = [1 2 1 2; 3 3 3 3; 0 2 1 5; 2 1 3 1]

[L,U] = doolittle(A)

// L =
//   1.   0.          0.   0.
//   3.   1.          0.   0.
//   0.  -0.6666667   1.   0.
//   2.   1.          1.   1.

// U =
//   1.   2.   1.   2.
//   0.  -3.   0.  -3.
//   0.   0.   1.   3.
//   0.   0.   0.  -3.
