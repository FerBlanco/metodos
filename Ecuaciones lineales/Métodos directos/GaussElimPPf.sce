clc
clear

// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana con pivoteo parcial.
function [x,a] = gausselimPP(A,b)
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    a = [A b]; // Matriz aumentada
    n = nA;
    // Eliminación progresiva
    for k=1:n-1
        
        // Pivoteo
        kpivot = k; amax = abs(a(k,k));  
        for i=k+1:n
            if abs(a(i,k))>amax then
                kpivot = i; amax = abs(a(i,k));
            end;
        end;
        
    temp = a(kpivot,:); a(kpivot,:) = a(k,:); a(k,:) = temp;
    
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik=a(i,k)/a(k,k);
            a(i,k+1:n+1) = a(i,k+1:n+1)-a(k,k+1:n+1)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1:n)*x(i+1:n))/a(i,i)
    end;
endfunction

// Ejemplo de aplicación
A = [6 2 2; 2 0.6667 0.3333; 1 2 -1]
b = [-2 1 0]'

[x,a] = gausselimPP(A,b)
disp(x)
//   2.599928
//  -3.799904
//  -4.99988

disp(a)
//   6.   2.          2.         -2.       
//   0.   1.6666667  -1.3333333   0.3333333
//   0.   0.         -0.33334     1.66666  

A2 = [0 1 2 -1; 1 2 2 0; -1/3 2 0 0; 2 1 1 2]
b2 = [1 3 2 1]'

[x2,a2] = gausselimPP(A2,b2)
disp(a2,x2)
//   3.
//   1.5
//  -1.5
//  -2.5

//   2.   1.          1.          2.          1.       
//   0.   2.1666667   0.1666667   0.3333333   2.1666667
//   0.   0.          1.9230769  -1.1538462   0.       
//   0.   0.          0.         -0.4         1.       

A3 = [1 2 -2 1; 4 5 -7 6; 5 25 -15 -3; 6 -12 -6 22]
b3 = [1 2 0 1]'

[x3,a3] = gausselimPP(A3,b3)
disp(a3,x3)

//   9.8333333
//  -6.1666667
//  -5.5
//  -7.5

//   6.  -12.  -6.          22.         1.       
//   0.   35.  -10.        -21.333333  -0.8333333
//   0.   0.    0.7142857  -0.7428571   1.6428571
//   0.   0.    0.         -0.08        0.6      

