clc
clear

// Factorización QR
// A una matriz con columnas linealmente independientes
// Q una matriz con columnas ortogonales
// R una matriz triangular superior
// tal que se cumple A = QR

// La función realiza la factorización QR
// dada una matriz A de columnas linealmente independientes
function [Q,R] = QR(A)
    [n,m] = size(A);
    Q = zeros(n,m);
    R = zeros(m,m);
    
    // Paso inicial
    R(1,1) = norm(A(:,1)); // R(k,k) = v(k)
    Q(:,1) = A(:,1)/R(1,1)
    R(1,2:m) = (A(:,2:m)'*Q(:,1))
    
    for k = 2:m
        sumk = 0;
        for i = 1:k-1
            sumk = sumk + (A'(k,:)*Q(:,i))*Q(:,i);
        end;
        resk = A(:,k)-sumk;
        R(k,k) = norm(resk);
        Q(:,k) = (resk)/R(k,k);
        if k<m then
            R(k,k+1:m) = (A(:,k+1:m)'*Q(:,k))
        end;
    end;
endfunction

// Casos de prueba

A1 = [2 3 2; 1 3 5; 5 1 5]
[Q1,R1] = QR(A1)
disp(R1,Q1,A1)

// Q =
//   0.3651484   0.5853226  -0.7239228
//   0.1825742   0.7174922   0.672214 
//   0.9128709  -0.3776275   0.1551263

// R =
//   5.4772256   2.5560386   6.2075223
//   0.          3.5308167   2.8699687
//   0.          0.          2.688856 

// Q*R=
//   2.   3.   2.
//   1.   3.   5.
//   5.   1.   5.

A2 = [12 -51 4; 6 167 -68; -4 24 -41]
[Q2,R2] = QR(A2)
disp(R2,Q2,A2)

// Q =
//   0.8571429  -0.3942857  -0.3314286
//   0.4285714   0.9028571   0.0342857
//  -0.2857143   0.1714286  -0.9428571

// R =
//   14.   21.   -14.
//   0.    175.  -70.
//   0.    0.     35.

// Q*R = 
//   12.  -51.    4. 
//   6.    167.  -68.
//  -4.    24.   -41.
