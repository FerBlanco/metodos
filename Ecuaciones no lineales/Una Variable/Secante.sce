// Método de la secante (Algoritmo de la resolución)
// f es un función
// a y b son puntos 
// eps es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = m_secante(f,a,b,eps,maxIter)
    iter = 1
    fb = f(b)
    fa = f(a)
    actual = b - fb*(b-a)/(fb-fa);
    anterior = b;
    while (abs(f(actual)) >= eps) && (iter < maxIter)
        // actual = xn
        // anterior = x(n-1)
        // siguiente = x(n+1)
        fant = f(anterior);
        fact = f(actual);
        siguiente = actual - fact * (actual-anterior)/(fact-fant);
        anterior = actual;
        actual = siguiente;
        iter = iter + 1;
    end
    if iter == maxIter
        x = %nan
    else x = actual
    end
endfunction

// Prueba
// deff("y=f(x)","y=log10(x)-x+1")
// m_secante(f,0.2,2,0.0001,10)
// ans  = 1.0000089
