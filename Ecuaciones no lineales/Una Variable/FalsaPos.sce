// Método de la falsa posición
// f es una función 
// a y b son dos puntos tal que f es continua en [a,b] y f(a)f(b)<=0
// e es la tolerancía al error
function x = m_falsapos(f,a,b,e)
    if f(a)*f(b)>0 
        then c = %nan
    else
        c = (a+b)/2;
        if ~(f(c) == 0) 
            then 
            if f(a)*f(c) < 0 
                then b = c;
            else a = c;
            end
            actual = b - f(b)*(b-a)*(f(b)-f(a));
            anterior = b;
            while(abs(f(c)>e)
                // actual = xn
                // anterior = x(n-1)
                // siguiente = x(n+1)
                fant = f(anterior);
                fact = f(actual);
                siguiente = actual - fact * (actual-anterior)/(fact-fant);
                anterior = actual;
                actual = siguiente;  
            end
            c = actual;
endfunction
