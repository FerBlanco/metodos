clc
clear 
// Método de Newton
// f es una función derivable
// x0 es un punto 
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = m_newton(f,x0,e,maxIter)
    iter = 0;
    x = x0-f(x0)/numderivative(f,x0);
    while (abs(x-x0)>e) && (iter < maxIter)
        x0 = x;
        x=x0-f(x0)/numderivative(f,x0);
        iter=iter+1;
    end;
    if (abs(x-x0)>e) 
        then x = %nan
    end
endfunction

// Prueba
//deff("y=p(x)","y=x^2*cos(x)")
//r = m_newton(p,1.5,0.0001,50)
//disp(r)
// r  = 1.5707963

deff("y=f(x)","y=cos(x)*cosh(x)+1");
r = m_newton(f,0,2,0.00001)
disp(r)
