// Blanco Fernando
clc
clear 
// Método de Steffeson
// f es una función derivable
// x0 es un punto 
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
// Esta función calcula una raíz de la función f 
// mediante el método de Steffeson
function x = m_steffeson(f,x0,e,maxIter)
    iter = 0;
    D = (f(x0+f(x0))-f(x0))/f(x0);
    x = x0-f(x0)/D;
    while (abs(x-x0)>e) && (iter < maxIter)
        x0 = x;
        D = (f(x0+f(x0))-f(x0))/f(x0);
        x=x0-f(x0)/D;
        iter=iter+1;
    end;
    if (abs(x-x0)>e) 
        then x = %nan
    end
endfunction

// Prueba
disp("Casos de prueba")

deff("y=p(x)","y=x^2*cos(x)")
r = m_steffeson(p,1.5,0.0001,50)
disp(r)
// r  = 1.5707963

deff("y=f(x)","y=cos(x)*cosh(x)+1");
r = m_steffeson(f,0,0.00001,50)
disp(r)
// r =  1.8751041

deff("y=g(x)","y=log10(x)-x+1")
r = m_steffeson(g,2,0.0001,10)
disp(r)
// r = 1.
