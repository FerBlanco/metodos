// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    //x = x0;
    x = g(x0)
    while abs(x-x0)>e && iter < maxIter
        x0 = x;
        x = g(x0);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
deff("y=f(x)","y=(%e^(2*x-6)+cos(x))");
plot([-3:0.1:5],f)
r = m_puntofijo(f,0.4,0.0001,30)
disp(r)
// ans  = 0.7456289
