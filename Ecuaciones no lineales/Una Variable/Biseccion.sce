// Método de la bisección
// f es una función 
// a y b son dos puntos tal que f es continua en [a,b] y f(a)f(b)<=0
// e es la tolerancía al error

function c = m_biseccion(f,a,b,e)
    if f(a)*f(b)>0 
        then 
            error("No cumple con el teorema de bolsano")
            abort
    else c = (a+b)/2;
         while (b-c>e)
             if f(b)*f(c) <= 0 
                 then a = c;
             else b = c;
             end
         c = (a+b)/2;
     end
     end
endfunction

// prueba del método
//deff("y=f(x)","y=cos(x)*cosh(x)+1");
// m_biseccion(f,0,3,0.00001)
// ans  =  1.8751087

deff("y=p(x)","y=x^2*cos(x)")
r = m_biseccion(p,1.5,0.0001,50)
disp(r)
