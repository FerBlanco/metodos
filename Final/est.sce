/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction

/*
Metodo del Simpson compuesto
Aproxima el una integral por medio de la regla del Simpson compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = simpsonp_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a) + f(b);
    for i=1:n-1
        if pmodulo(i,2) == 0 then
            s = s+2*f(a+i*h)
        else
            s = s+4*f(a+i*h)
        end
    end
    y = (h/3)*s;
endfunction

///////////////////////////////////////////////////////

//deff("y=I(x)","y=%e^(2*x)")
//deff("y=I(x)","y=1/x")

function d = TaylorFun(f,v,x,n,h)
    deff("y=DF0(x)","y="+f);
    deff("y=T0(x)","y=DF0("+string(v)+")");
    if n == 0 then d = DF0(v);
       else
    for i = 1:(n-1)
        deff("y=DF"+string(i)+"(x)","y=(DF"+string(i-1)+"(x+"+string(h)+")-DF"+string(i-1)+"(x))/"+string(h));
        deff("y=T"+string(i)+"(x)","y = T"+string(i-1)+"(x) + DF"+string(i)+"("+string(v)+")*(x-"+string(v)+")* (1/factorial("+string(i)+"))")
    end
    deff("y=DFn(x)","y=(DF"+string(n-1)+"(x+"+string(h)+")-DF"+string(n-1)+"(x))/"+string(h));
    deff("y=Tn(x)","y = T"+string(n-1)+"(x) + DFn("+string(v)+")*(x-"+string(v)+")* (1/factorial("+string(n)+"))")
    
    d = Tn(x);    
    end
endfunction

f = "(8+x)^(1/3) - 2";
deff("y=fg(x)","y=(8+x)^(1/3) - 2")
a = linspace(-1*10^(-12),1*10^(-12));
b = linspace(-1*10^(-13),1*10^(-13));
c = linspace(-1*10^(-14),1*10^(-14));

for i=1:100
    ta(i) =  TaylorFun(f,0,a(i),3,0.001)
    tb(i) =  TaylorFun(f,0,b(i),3,0.001)
    tc(i) =  TaylorFun(f,0,c(i),3,0.001)
end


plot(c,tc)
plot(c,fg,"r")
