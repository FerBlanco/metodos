clc
clear

//////////// Ejercicio 3///////////////////

// Esta función obtiene la solución de los sistemas de ecuaciones lineales 
// A*x1=b1, A*x2=b2 y A*x3=b3
// dada la matriz de coeficientes A y la matriz B = [b1,b2,b3].
// La función implementa el método de Eliminación Gaussiana sin pivoteo.

function [x,a] = gausselimm(A,B)
    [nA,mA] = size(A)
    [nb,mb] = size(B)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    
    a = [A B]; // Matriz aumentada
    n = nA;
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik = a(i,k)/a(k,k);
            a(i,k+1:n+mb) = a(i,k+1:n+mb)-a(k,k+1:n+mb)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n,:) = a(n,n+1:n+mb)/a(n,n);
    
    for i = n-1:-1:1
        x(i,:) = (a(i,n+1:n+mb)-a(i,i+1:n)*x(i+1:n,:))/a(i,i)
    end;
endfunction

// Ejemplo de aplicación
/*
A = [1 2 3 4; 2 2 2 2; 3 2 2 3; 2 4 3 4]
b = [2 1 2 1; 3 3 3 3; 4 2 1 5; -1 1 -1 1]
x = gausselimm(A,b)
//x  =
//   2.    1.    0.8   2.2
//  -2.5  -0.5  -1.9  -1.1
//   3.    3.    5.4   0.6
//  -1.   -2.   -2.8  -0.2
*/
// b) 

A = [1 2 3; 3 -2 1;4 2 -1]
B = [14 9 -2; 2 -5 2; 5 19 12]

[x,a] = gausselimm(A,B)

disp("La matriz resultante del método es")
disp(a)
disp("Y el resulta es x =")
disp(x)

// a =
//  1.   2.   3.   14.   9.   -2. 
//  0.  -8.  -8.  -40.  -32.   8. 
//  0.   0.  -7.  -21.   7.    14.

// x =
//   1.   2.   2.
//   2.   5.   1.
//   3.  -1.  -2.


// c) 
// Se sabe que A*A^(-1) = I
// Por lo tanto para calcular la inversa puedo definir el sistema 
// Ax=I y utilizar el metodo de Gauss modificado

[n,m] = size(A)
[Ainv,a] = gausselimm(A,eye(n,m))

disp("La inversa de A es")
disp(Ainv)

// A^(-1) = 
//   0.      0.1428571   0.1428571
//   0.125  -0.2321429   0.1428571
//   0.25    0.1071429  -0.1428571
