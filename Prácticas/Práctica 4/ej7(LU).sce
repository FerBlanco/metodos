// Ejercicio 7

// Gauss con PP factorización PA = LU
// La función gaussLU toma una matriz A 
// y calcula su factorización PA = LU 
// usando la eliminación de Gauss con pivoteo parcial

function [P,L,U]=gaussLU(A)
    [n,m] = size(A) 
    if n<>m then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    U = A;
    L = eye(m,m);
    P = L;
    for k = 1:m-1
        // Buscar maximo U(i,k) (pivot)
        amax = abs(U(k,k));
        kpivot = k;
        for i = k+1:m
            if abs(U(i,k)) > amax
                then amax = abs(U(i,k));
                     kpivot = i; 
            end;
        end;
        
        //Pivoteo en U
        temp = U(k,k:m);
        U(k,k:m) = U(kpivot,k:m);
        U(kpivot,k:m) = temp;

        //Pivoteo en L
        temp = L(k,1:k-1);
        L(k,1:k-1) = L(kpivot,1:k-1);
        L(kpivot,1:k-1) = temp;
        
        //Pivoteo en P
        temp = P(k,:)
        P(k,:) = P(kpivot,:)
        P(kpivot,:) = temp;
        
        //Eliminación de Gauss
        for j = k+1:m
            // L(j,k) = mjk
            L(j,k) = U(j,k)/U(k,k);
            U(j,k:m) = U(j,k:m) - L(j,k)*U(k,k:m);
        end;
    end;
endfunction

A = [2 1 1 0; 4 3 3 1; 8 7 9 5; 6 7 9 8]

[P,L,U] = gaussLU(A);

// L =
//   1.     0.          0.          0.
//   0.75   1.          0.          0.
//   0.5   -0.2857143   1.          0.
//   0.25  -0.4285714   0.3333333   1.

// U =
//   8.   7.     9.          5.       
//   0.   1.75   2.25        4.25     
//   0.   0.    -0.8571429  -0.2857143
//   0.   0.     0.          0.6666667

// P =
//    0.   0.   1.   0.
//    0.   0.   0.   1.
//    0.   1.   0.   0.
//    1.   0.   0.   0.

A2 = [0 1 0 0; 1 2 3 0; 0 1 0 2; 0 0 -1 -1]

[P2,L2,U2] = gaussLU(A2)

// L =
//   1.   0.   0.   0.
//   0.   1.   0.   0.
//   0.   0.   1.   0.
//   0.   1.   0.   1.

// U =
//   1.   2.   3.   0.
//   0.   1.   0.   0.
//   0.   0.  -1.  -1.
//   0.   0.   0.   2.

// P =
//   0.   1.   0.   0.
//   1.   0.   0.   0.
//   0.   0.   0.   1.
//   0.   0.   1.   0.
