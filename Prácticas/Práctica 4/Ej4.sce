clc
clear

//////////// Ejercicio 4 //////////////

// Esta función calcula el determinante de una matriz A
// La función implementa la eliminación gausseana para obtener la matriz
// triángular superior y luego multiplica los elementos de su diagonal

function [x,a] = gaussdet(A)
    [nA,mA] = size(A)
    
    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    a = A; // Matriz aumentada
    n = nA;
    
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            a(i,:) = a(i,:)-a(k,:)*a(i,k)/a(k,k);
        end;
    end;
    
    // Calculo del determinante
    x = prod(diag(a));
    
endfunction

// Ejemplo de aplicación
/*
A = [1 2 3 4; 2 -2 2 2; 3 2 2 3; 2 4 -3 4]
[d,a] = gaussdet(A)
// a  = 
//   1.   2.   3.          4.       
//   0.  -6.  -4.         -6.       
//   0.   0.  -4.3333333  -5.       
//   0.   0.   0.          6.3846154

// d  = 166.
*/

A = [1,2,3;3,-2,1;4,2,-1]
[x,a] = gaussdet(A)

disp("El determinante de A es")
disp(x)

// x = 56
