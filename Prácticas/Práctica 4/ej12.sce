clc
clear

function [U,ind] = cholesky(A)
// Factorización de Cholesky.
// Trabaja únicamente con la parte triangular superior.
//
// ind = 1  si se obtuvo la factorización de Cholesky.
//     = 0  si A no es definida positiva
//
//******************
eps = 1.0e-8
//******************

n = size(A,1)
U = zeros(n,n)

t = A(1,1)

if t <= eps then
    printf('Matriz no definida positiva.\n')
    ind = 0
    return
end

U(1,1) = sqrt(t);

// Primera fila de U
for j = 2:n
    U(1,j) = A(1,j)/U(1,1)
end
    
for k = 2:n
    t = A(k,k) - U(1:k-1,k)'*U(1:k-1,k)
    
    if t <= eps then
        printf('Matriz no definida positiva.\n')
        ind = 0
        return
    end
    
    U(k,k) = sqrt(t)
    
    for j = k+1:n
        U(k,j) = ( A(k,j) - U(1:k-1,k)'*U(1:k-1,j) )/U(k,k)
    end
end
ind = 1

endfunction

// Ejemplo de aplicación
/*
B = [5 2 1 0; 2 5 2 0; 1 2 5 2; 0 0 2 5]
[Ub,ind] = cholesky(B)


// U =
//   2.236068   0.8944272   0.4472136   0.       
//   0.         2.0493902   0.7807201   0.       
//   0.         0.          2.0470653   0.9770084
//   0.         0.          0.          2.0113315

// Ub'*Ub
// ans  =
//   5.   2.   1.   0.
//   2.   5.   2.   0.
//   1.   2.   5.   2.
//   0.   0.   2.   5.

C = [5 2 1 0; 2 -4 2 0; 1 2 2 2; 0 0 2 5]
disp(C)
[Uc,ind] = cholesky(C)
disp(ind,Uc)
// ind = 0
// Matriz no definida positiva

D = [3 2 1 0; 
     2 3 2 1;
     1 2 3 2;
     0 1 2 3]
[Ud,ind] = cholesky(D)
disp(Ud,D)

// D =
//   3.   2.   1.   0.
//   2.   3.   2.   1.
//   1.   2.   3.   2.
//   0.   1.   2.   3.

// Ud =
//   1.7320508   1.1547005   0.5773503   0.       
//   0.          1.2909944   1.0327956   0.7745967
//   0.          0.          1.2649111   0.9486833
//   0.          0.          0.          1.2247449
*/

// Sustición regresiva
// Sea Ax=b un sistema triángular superior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución regresiva

function x = remonte(A,b)
    n = size(A,1);
    x(n) = b(n)/A(n,n);
    for i = n-1:-1:1
       x(i) = (b(i)-A(i,i+1:n)*x(i+1:n))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
//A = [1,2,3;0,1,3;0,0,2]
//b = [2;2;2]
//remonte(A,b)
// ans  = 
//    1.
//   -1.
//    1.


// Sustición progresiva
// Sea Ax=b un sistema triángular inferior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución progresiva

function x = sustpro(A,b)
    n = size(A,1);
    x(1) = b(1)/A(1,1);
    for i = 2:n
       x(i) = (b(i)-A(i,1:i-1)*x(1:i-1))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
//sustpro(A',b)
// ans  = 
//    2.
//   -2.
//    1.

/////////////////// Ejercicio 12 /////////////////

A = [16 -12 8; -12 18 -6; 8 -6 8]
b = [76 -66 46]'

R = cholesky(A)
disp(R)

// R =
//   4.  -3.   2.
//   0.   3.   0.
//   0.   0.   2

y = sustpro(R',b)
disp(y)

// y =
//   19.
//  -3.
//   4.

x = remonte(R,y)
disp(x)

// x = 
//   3.
//  -1.
//   2.
