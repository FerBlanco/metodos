clc
clear

// Ejercicio 6

// Esta función resuelve el sistema de ecuaciones A*x=b
// donde A es una matriz tridiagonal y b un vector
// La función aplica el método de Gauss modificado para  no realizar 
// calculos donde se sabe que da 0 por la forma de la matriz
function [x,a] = gausselimtri(A,b)
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    
    a = [A b]; // Matriz aumentada
    n = nA;
    c = 0; // Contador
    
    // Eliminación progresiva
    for k=1:n-1
        // Ei = Ei - mik*Ek
        // mik = a(i,k)/a(k,k)
        // si i > k+1 -> a(i,k) = 0 = mik
        // si i > k+1 -> a(k,i) = 0
        mik = a(k+1,k)/a(k,k)
        a(k+1,k+1) = a(k+1,k+1)-a(k,k+1)*mik;
        a(k+1,n+1) = a(k+1,n+1)-a(k,n+1)*mik;
        a(k+1,k)=0;
        c = c+5;
        end;
    
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    c = c+1;
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1)*x(i+1))/a(i,i)
        c = c + 3;
    end;
    printf("Se realizaronn %d operaciones\n",c)
endfunction

// Caso de prueba
/*
A = [1,2,0,0;1,1,3,0;0,2,1,1;0,0,3,5]
b = [1,2,3,4]'

[x,a]=gausselimtri(A,b)
disp(x,a)
// En este caso de prueba se realizaron 25 operaciones

// x = 
//  -0.9375
//   0.96875
//   0.65625
//   0.40625

//   1.   2.   0.   0.          1.       
//   0.  -1.   3.   0.          1.       
//   0.   0.   7.   1.          5.       
//   0.   0.   0.   4.5714286   1.8571429

A2=[1 -1 0 0; 1 -1 3 0; 0 3 1 2; 0 0 4 1]
b2 = [3 3 3 3]'

[x2,a2] = gausselimtri(A2,b2)
disp(a2,x2,A2)
// x =
//   Nan
//   Nan
//   0.
//   3.

// a =
//   1.  -1.   0.    0.   3.
//   0.   0.   3.    0.   0.
//   0.   0.  -Inf   2.   3.
//   0.   0.   0.    1.   3.

// Vemos que para esta matriz se necesita hacer pivoteo 
// debido a que se nos hace un 0 en a(2,2)
*/

// Esta función resuelve el sistema de ecuaciones A*x=b
// donde A es una matriz tridiagonal y b un vector
// La función aplica el método de Gauss con pivoteo 
// modificado para  no realizar calculos donde se sabe que da 
// 0 por la forma de la matriz
function [x,a] = gausselimtriPP(A,b)
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    
    a = [A b]; // Matriz aumentada
    n = nA;
    c = 0; // Contador
    
    // Eliminación progresiva
    for k=1:n-1
        // Pivoteo
        if abs(a(k+1,k))>a(k,k) 
            then temp = a(k+1,:);
                 a(k+1,:) = a(k,:); 
                 a(k,:) = temp;
        end;
        
        // Ei = Ei - mik*Ek
        // mik = a(i,k)/a(k,k)
        // si i > k+1 -> a(i,k) = 0 = mik
        // si i > k+1 -> a(k,i) = 0
        mik = a(k+1,k)/a(k,k)
        //a(k+1,k+1) = a(k+1,k+1)-a(k,k+1)*mik;
        //a(k+1,n+1) = a(k+1,n+1)-a(k,n+1)*mik;
        a(k+1,(k+1):(n+1)) = a(k+1,(k+1):(n+1))-a(k,(k+1):(n+1))*mik;
        a(k+1,k)=0;
        c = c+2*(n-k)+1;
        end;
    
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    x(n-1) = (a(n-1,n+1)-a(n-1,n)*x(n))/a(n-1,n-1)
    c = c+4;
    for i = n-2:-1:1
        x(i) = (a(i,n+1)-a(i,i+1)*x(i+1)-a(i,i+2)*x(i+2))/a(i,i)
        c = c + 5;
    end;
    printf("Se realizaron %d operaciones\n",c)
endfunction

// Ejemplo de aplicación
/*
[x,a]=gausselimtriPP(A,b)
// En este caso de prueba se realizaron 29 operaciones

// x = 
//  -0.9375
//   0.96875
//   0.65625
//   0.40625

// a =
//   1.   2.   0.    0.          1.       
//   0.   2.   1.    1.          3.       
//   0.   0.   3.5   0.5         2.5      
//   0.   0.   0.    4.5714286   1.8571429

[x2,a2] = gausselimtriPP(A2,b2)

// x = 
//   2.
//  -1.
//   0.
//   3.

// a =
//   1.  -1.   0.   0.     3.  
//   0.   3.   1.   2.     3.  
//   0.   0.   4.   1.     3.  
//   0.   0.   0.  -0.75  -2.25

A3 = [0 1 0 0; 1 2 3 0; 0 1 0 2; 0 0 -1 -1]
b3 = [1 2 2 1]'

[x3,a3] = gausselimtriPP(A3,b3)

// x = 
//   4.5
//   1.
//  -1.5
//   0.5

// a =
//   1.   2.   3.   0.   2.
//   0.   1.   0.   0.   1.
//   0.   0.  -1.  -1.   1.
//   0.   0.   0.   2.   1.
*/
