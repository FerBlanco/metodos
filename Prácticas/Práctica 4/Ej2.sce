clc
clear

function [x,a] = gausselim(A,b)
// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana sin pivoteo.  

[nA,mA] = size(A) 
[nb,mb] = size(b)

if nA<>mA then
    error('gausselim - La matriz A debe ser cuadrada');
    abort;
elseif mA<>nb then
    error('gausselim - dimensiones incompatibles entre A y b');
    abort;
end;

a = [A b]; // Matriz aumentada
c = 0;
// Eliminación progresiva
n = nA;
for k=1:n-1
    for i=k+1:n
        for j=k+1:n+1
            //a(i,k)/a(k,k) = m(i,k)
            // a(i,j) = a(i,j)-a(k,j)m(i,k) = a(i,j)-a(k,j)*a(i,k)/a(k,k)
            a(i,j) = a(i,j) - a(k,j)*a(i,k)/a(k,k); 
            c = c + 3; // 1 resta, 1 producto y 1 división
        end;
        for j=1:k        // no hace falta para calcular la solución x
            a(i,j) = 0;  // no hace falta para calcular la solución x
        end              // no hace falta para calcular la solución x
    end;
end;

// Sustitución regresiva
x(n) = a(n,n+1)/a(n,n);
for i = n-1:-1:1
    sumk = 0
    for k=i+1:n
        sumk = sumk + a(i,k)*x(k);
        c = c + 2 // 1 suma y 1 producto
    end;
    x(i) = (a(i,n+1)-sumk)/a(i,i);
    c = c + 2 // 1 resta y 1 división
end;
printf("Se realizaron %d operaciones\n",c)
endfunction

// Casos de prueba
/*
A = [2 -3 4; 2 2 2; -2 -7 1]
b = [3 2 3]'
[x,a] = gausselim(A,b)
disp("Casos de prueba de eliminación de Gauss")
disp(x)
//  -4.4
//   1.4
//    4.

disp(a)
//   2.  -3.   4.   3.
//   0.   5.  -2.  -1.
//   0.   0.   1.   4.
*/
// Ejercicio 2
// b)

A = [1 1 0 3; 2 1 -1 1; 3 -1 -1 2; -1 2 3 -1]
b = [4 1 -3 4]'

[x,a] = gausselim(A,b)
disp(a)
disp(x)

// x = [-1,2,0,1]' 


A2 = [1 -1 2 -1; 2 -2 3 -3; 1 1 1 0; 1 -1 4 3]
b2 = [-8 -20 -2 4]'

[x2,a2] = gausselim(A2,b2)

disp(a2)
disp(x2)

// x = [Nan,Nan,Nan,Nan]'
// En el primer paso m21 = 2
// a(2,2) = a(2,2)-m(2,1)*a(1,2)
// a(2,2) = -2 - 2*(-1) = -2 + 2 = 0
// Por lo tanto nos deja un 0 en la diagonal imposibilitanto 
// el uso del método sin pivoteo


A3 = [1,1,0,4;2,1,-1,1;4,-1,-2,2;3,-1,-1,2]
b3 = [2,1,0,-3]'

[x3,a3] = gausselim(A3,b3)

disp(a3)
disp(x3)

// x = [-4,0.6666667,-7,1.3333333]


// d)

function [x,a] = gausselimc(A,b)
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    
    
    a = [A b]; // Matriz aumentada
    n = nA;
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik=a(i,k)/a(k,k);
            a(i,k+1:n+1) = a(i,k+1:n+1)-a(k,k+1:n+1)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1:n)*x(i+1:n))/a(i,i)
    end;
endfunction

// Casos de prueba
A = [2 -3 4; 2 2 2; -2 -7 1]
b = [3 2 3]'
[x,a] = gausselim(A,b)
disp("Casos de prueba de eliminación de Gauss")
disp(x)
//  -4.4
//   1.4
//    4.

disp(a)
//   2.  -3.   4.   3.
//   0.   5.  -2.  -1.
//   0.   0.   1.   4.
