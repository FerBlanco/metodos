// Ejercicio 1

// Sustición regresiva
// Sea Ax=b un sistema triángular superior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución regresiva

function x = remonte(A,b)
    n = size(A,1);
    x(n) = b(n)/A(n,n);
    for i = n-1:-1:1
       x(i) = (b(i)-A(i,i+1:n)*x(i+1:n))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
A = [1,2,3;0,1,3;0,0,2]
b = [2;2;2]
//remonte(A,b)
// ans  = 
//    1.
//   -1.
//    1.


// Sustición progresiva
// Sea Ax=b un sistema triángular superior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución progresiva

function x = sustpro(A,b)
    n = size(A,1);
    x(1) = b(1)/A(1,1);
    for i = 2:n
       x(i) = (b(i)-A(i,1:i-1)*x(1:i-1))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
//sustpro(A',b)
// ans  = 
//    2.
//   -2.
//    1.
