clc
clear

function [x,a,P,L] = gausselimPP(A,b)
// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana con pivoteo parcial.

[nA,mA] = size(A) 
[nb,mb] = size(b)

if nA<>mA then
    error('gausselim - La matriz A debe ser cuadrada');
    abort;
elseif mA<>nb then
    error('gausselim - dimensiones incompatibles entre A y b');
    abort;
end;

a = [A b]; // Matriz aumentada
n = nA;    // Tamaño de la matriz
P = eye(n,n);
L = P;
// Eliminación progresiva con pivoteo parcial
for k=1:n-1
        
        // Pivoteo
        pivot = k;   
        for i=k+1:n
            if abs(a(i,k))>abs(a(pivot,k)) then
                pivot = i;
            end;
        end;
        
        temp = a(pivot,:); a(pivot,:) = a(k,:); a(k,:) = temp;
        temp = P(pivot,:); P(pivot,:) = P(k,:); P(k,:) = temp;
        if k>1 then
            temp = L(pivot,1:k-1); L(pivot,1:k-1) = L(k,1:k-1); L(k,1:k-1) = temp;
        end;
        
        for i=k+1:n
            // Ei = Ei - mik*Ek
            L(i,k)=a(i,k)/a(k,k);
            a(i,k+1:n+1) = a(i,k+1:n+1)-L(i,k)*a(k,k+1:n+1);
            a(i,k)=0
        end;
        
    end;
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1:n)*x(i+1:n))/a(i,i)
    end;
endfunction


// Sustición regresiva
// Sea Ax=b un sistema triángular superior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución regresiva

function x = remonte(A,b)
    n = size(A,1);
    x(n) = b(n)/A(n,n);
    for i = n-1:-1:1
       x(i) = (b(i)-A(i,i+1:n)*x(i+1:n))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
/*
A = [1,2,3;0,1,3;0,0,2]
b = [2;2;2]
remonte(A,b)
// ans  = 
//    1.
//   -1.
//    1.
*/

// Sustición progresiva
// Sea Ax=b un sistema triángular inferior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución progresiva

function x = sustpro(A,b)
    n = size(A,1);
    x(1) = b(1)/A(1,1);
    for i = 2:n
       x(i) = (b(i)-A(i,1:i-1)*x(1:i-1))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
/*
A = [1,2,3;0,1,3;0,0,2]
sustpro(A',b)
// ans  = 
//    2.
//   -2.
//    1.
*/

//////////////////////// Ejercicio 9 ////////////////////

A = [1 2 -2 1; 4 5 -7 6; 5 25 -15 -3; 6 -12 -6 22]
b = [1 2 0 1]'

[x,a,P,L] = gausselimPP(A,b);
U = a(:,1:size(A,1))

disp("a)")
disp("L =")
disp(L)
disp("U =")
disp(U)
disp("P =")
disp(P)
disp("x =")
disp(x)

// L
//   1.          0.          0.    0.
//   0.8333333   1.          0.    0.
//   0.6666667   0.3714286   1.    0.
//   0.1666667   0.1142857   0.2   1.

// U
//   6.  -12.  -6.          22.      
//   0.   35.  -10.        -21.333333
//   0.   0.    0.7142857  -0.7428571
//   0.   0.    0.         -0.08     



// P
//   0.   0.   0.   1.
//   0.   0.   1.   0.
//   0.   1.   0.   0.
//   1.   0.   0.   0.

// x
//   9.8333333
//  -6.1666667
//  -5.5
//  -7.5


// b)

// Para resolver los sistemas usaremos los siguientes algoritmos de 
// sustitución progresiva y regresiva


b2 = [2 2 1 0]'

// LUx = Pb
// Ly = Pb   
// Ux = y


y2 = sustpro(L,P*b2)
x2 = remonte(U,y2)

disp("b)")
disp("Ux = ")
disp(y2)
disp("x =")
disp(x2)

// y2  = 
//   0.
//   1.
//   1.6285714
//   1.56

// x2 = 
//   19.5
//  -17.
//  -18.
//  -19.5
