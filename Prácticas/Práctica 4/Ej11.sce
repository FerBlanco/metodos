clc
clear

function [U,ind] = cholesky(A)
// Factorización de Cholesky.
// Trabaja únicamente con la parte triangular superior.
//
// ind = 1  si se obtuvo la factorización de Cholesky.
//     = 0  si A no es definida positiva
//
//******************
eps = 1.0e-8
//******************

n = size(A,1)
U = zeros(n,n)

t = A(1,1)

if t <= eps then
    printf('Matriz no definida positiva.\n')
    ind = 0
    return
end

U(1,1) = sqrt(t);

// Primera fila de U
for j = 2:n
    U(1,j) = A(1,j)/U(1,1)
end
    
for k = 2:n
    t = A(k,k) - U(1:k-1,k)'*U(1:k-1,k)
    
    if t <= eps then
        printf('Matriz no definida positiva.\n')
        ind = 0
        return
    end
    
    U(k,k) = sqrt(t)
    
    for j = k+1:n
        U(k,j) = ( A(k,j) - U(1:k-1,k)'*U(1:k-1,j) )/U(k,k)
    end
end
ind = 1

endfunction

// Ejemplo de aplicación
/*
B = [5 2 1 0; 2 5 2 0; 1 2 5 2; 0 0 2 5]
[Ub,ind] = cholesky(B)


// U =
//   2.236068   0.8944272   0.4472136   0.       
//   0.         2.0493902   0.7807201   0.       
//   0.         0.          2.0470653   0.9770084
//   0.         0.          0.          2.0113315

// Ub'*Ub
// ans  =
//   5.   2.   1.   0.
//   2.   5.   2.   0.
//   1.   2.   5.   2.
//   0.   0.   2.   5.

C = [5 2 1 0; 2 -4 2 0; 1 2 2 2; 0 0 2 5]
disp(C)
[Uc,ind] = cholesky(C)
disp(ind,Uc)
// ind = 0
// Matriz no definida positiva

D = [3 2 1 0; 
     2 3 2 1;
     1 2 3 2;
     0 1 2 3]
[Ud,ind] = cholesky(D)
disp(Ud,D)

// D =
//   3.   2.   1.   0.
//   2.   3.   2.   1.
//   1.   2.   3.   2.
//   0.   1.   2.   3.

// Ud =
//   1.7320508   1.1547005   0.5773503   0.       
//   0.          1.2909944   1.0327956   0.7745967
//   0.          0.          1.2649111   0.9486833
//   0.          0.          0.          1.2247449

*/

/////////// Ejercicio 11 //////////////
disp("Ejercicio 11")
A = [16 -12 8 -16; -12 18 -6 9; 8 -6 5 -10; -16 9 -10 46]
B = [4 1 1; 8 2 2; 1 2 3]
C = [1 2; 2 4]


[Ua,d] = cholesky(A)
disp(Ua)
// U = 
//   4.  -3.   2.  -4.
//   0.   3.   0.  -1.
//   0.   0.   1.  -2.
//   0.   0.   0.   5.

A2 = Ua'*Ua
disp(A2)

// U'*U = 
//   16.  -12.   8.   -16.
//  -12.   18.  -6.    9. 
//   8.   -6.    5.   -10.
//  -16.   9.   -10.   46.

// Vemos que A es simetrica y la matriz U que nos devolvió el
// algoritmo de cholesky cumple que U'*U = A por lo que es definida positiva

[Ub,d] = cholesky(B)
disp(Ub)

// U = 
//   2.   0.5         0.5      
//   0.   1.3228757   1.3228757
//   0.   0.          1.   

B2 = Ub'*Ub
disp(B2)

// U'*U =
//   4.   1.   1.
//   1.   2.   2.
//   1.   2.   3.

// B no es una matriz simetrica por lo que la factorización de cholesky 
// no se cumple y el algoritmo nos retorna una matriz U que no cumple 
// con U'*U = B

[Uc,d] = cholesky(C)
disp(Uc)

// El algoritmo corta antes porque C no es definida positiva
// en el segundo paso nos queda t = 4 - 2*2 = 0 
