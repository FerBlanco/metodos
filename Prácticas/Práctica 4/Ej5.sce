clc
clear

function [x,a] = gausselimPP(A,b)
// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana con pivoteo parcial.

[nA,mA] = size(A) 
[nb,mb] = size(b)

if nA<>mA then
    error('gausselim - La matriz A debe ser cuadrada');
    abort;
elseif mA<>nb then
    error('gausselim - dimensiones incompatibles entre A y b');
    abort;
end;

a = [A b]; // Matriz aumentada
n = nA;    // Tamaño de la matriz

// Eliminación progresiva con pivoteo parcial
for k=1:n-1
    kpivot = k; amax = abs(a(k,k));  //pivoteo
    for i=k+1:n
        if abs(a(i,k))>amax then
            kpivot = i; amax = a(i,k);
        end;
    end;
    temp = a(kpivot,:); a(kpivot,:) = a(k,:); a(k,:) = temp;
    
    for i=k+1:n
        for j=k+1:n+1
            a(i,j) = a(i,j) - a(k,j)*a(i,k)/a(k,k);
        end;
        for j=1:k        // no hace falta para calcular la solución x
            a(i,j) = 0;  // no hace falta para calcular la solución x
        end              // no hace falta para calcular la solución x
    end;
end;

// Sustitución regresiva
x(n) = a(n,n+1)/a(n,n);
for i = n-1:-1:1
    sumk = 0
    for k=i+1:n
        sumk = sumk + a(i,k)*x(k);
    end;
    x(i) = (a(i,n+1)-sumk)/a(i,i);
end;
endfunction

// Ejemplo de aplicación
/*
disp("Casos de prueba de Gauss con pivoteo parcial")

// Ejemplo de aplicación
A2 = [0 2 3; -9 0 3; 8 16 -1]
b2 = [7 13 -3]'

[x2,a2] = gausselimPP(A2,b2)
disp(x2)

//  -0.7363184
//   0.3134328
//   2.1243781

disp(a2)
//   8.   16.  -1.         -3.       
//   0.   18.   1.875       9.625    
//   0.   0.    2.7916667   5.9305556


A = [6 2 2; 2 0.6667 0.3333; 1 2 -1]
b = [-2 1 0]'

[x,a] = gausselimPP(A,b)
disp(x)
//  2.599928
//  -3.799904
//  -4.99988

disp(a)
//   6.   2.          2.         -2.       
//   0.   1.6666667  -1.3333333   0.3333333
//   0.   0.         -0.33334     1.66666  

*/

/////////// Ejercicio 5 ////////////////
disp("Ejercicio 5")
A = [1 1 0 3; 2 1 -1 1; 3 -1 -1 2; -1 2 3 -1]
b = [4 1 -3 4]'

[x,a] = gausselimPP(A,b)
disp(a)
disp(x)

// x = 
//  -1.
//   2.
//   1.850D-17
//   1.


A2 = [1 -1 2 -1; 2 -2 3 -3; 1 1 1 0; 1 -1 4 3]
b2 = [-8 -20 -2 4]'

[x2,a2] = gausselimPP(A2,b2)

disp(a2)
disp(x2)

// x = 
//  -7.
//   3.
//   2.
//   2.

// Con el pivoteo se solucionó el error que teniamos 
// al intentar aplicar Gauss sólamente

A3 = [1,1,0,4;2,1,-1,1;4,-1,-2,2;3,-1,-1,2]
b3 = [2,1,0,-3]'

[x3,a3] = gausselimPP(A3,b3)

disp(a3)
disp(x3)

// x = 
//  -4.
//   0.6666667
//  -7.
//   1.3333333
