clc
clear

// Esta función obtiene la factorización A = LU
// dada la matriz A la función utiliza el metodo de Doolittle
// para obtener L y U
function [L,U]=doolittle(A)
    [n,m] = size(A);
    
    if n<>m then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    U = zeros(n,m);
    L = eye(n,m);
    
    U(1,:) = A(1,:); // Primera fila de U
    L(:,1) = A(:,1)/U(1,1); // Primera columna de L
    
    // Doolittle
    for i=2:n
        U(i,i:n) = A(i,i:n)-L(i,1:i-1)*U(1:i-1,i:n);
        L(i+1:n,i) = (A(i+1:n,i)-L(i+1:n,1:i-1)*U(1:i-1,i))/U(i,i);
    end;
endfunction

// Caso de prueba
/* 
Ap = [1 2 1 2; 3 3 3 3; 0 2 1 5; 2 1 3 1]

[Lp,Up] = doolittle(Ap)

// L =
//   1.   0.          0.   0.
//   3.   1.          0.   0.
//   0.  -0.6666667   1.   0.
//   2.   1.          1.   1.

// U =
//   1.   2.   1.   2.
//   0.  -3.   0.  -3.
//   0.   0.   1.   3.
//   0.   0.   0.  -3.
*/

// Sustición regresiva
// Sea Ax=b un sistema triángular superior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución regresiva

function x = remonte(A,b)
    n = size(A,1);
    x(n) = b(n)/A(n,n);
    for i = n-1:-1:1
       x(i) = (b(i)-A(i,i+1:n)*x(i+1:n))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
/*
A = [1,2,3;0,1,3;0,0,2]
b = [2;2;2]
remonte(A,b)
// ans  = 
//    1.
//   -1.
//    1.
*/

// Sustición progresiva
// Sea Ax=b un sistema triángular inferior 
// esta función devuelve la solución a dicho sistema
// La función implementa sustitución progresiva

function x = sustpro(A,b)
    n = size(A,1);
    x(1) = b(1)/A(1,1);
    for i = 2:n
       x(i) = (b(i)-A(i,1:i-1)*x(1:i-1))/A(i,i);
    end;
endfunction

// Ejemplo de aplicación
/*
A = [1,2,3;0,1,3;0,0,2]
sustpro(A',b)
// ans  = 
//    2.
//   -2.
//    1.
*/

///////////////// Ejercicio 10 ///////////////
 A = [1 2 3 4; 1 4 9 16; 1 8 27 64; 1 16 81 256]
 b = [2 10 44 190]'
 
 [L,U] = doolittle(A)
 disp(U,L)
 y = sustpro(L,b)
 disp(y)
 x = remonte(U,y)
 disp(x)
 
// L = 
//   1.   0.   0.   0.
//   1.   1.   0.   0.
//   1.   3.   1.   0.
//   1.   7.   6.   1.

// U =
//   1.   2.   3.   4. 
//   0.   2.   6.   12.
//   0.   0.   6.   24.
//   0.   0.   0.   24.

// y =
//   2.
//   8.
//   18.
//   24.

// x =
//  -1.
//   1.
//  -1.
//   1.
