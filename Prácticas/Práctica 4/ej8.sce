clc
clear 
// Gauss con PP factorización PA = LU
// La función gaussLU toma una matriz A 
// y calcula su factorización PA = LU 
// usando la eliminación de Gauss con pivoteo parcial

function [P,L,U]=gaussLU(A)
    [n,m] = size(A) 
    if n<>m then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    U = A;
    L = eye(m,m);
    P = L;
    for k = 1:m-1
        // Buscar maximo U(i,k) (pivot)
        amax = abs(U(k,k));
        kpivot = k;
        for i = k+1:m
            if abs(U(i,k)) > amax
                then amax = abs(U(i,k));
                     kpivot = i; 
            end;
        end;
        
        //Pivoteo en U
        temp = U(k,k:m);
        U(k,k:m) = U(kpivot,k:m);
        U(kpivot,k:m) = temp;

        //Pivoteo en L
        temp = L(k,1:k-1);
        L(k,1:k-1) = L(kpivot,1:k-1);
        L(kpivot,1:k-1) = temp;
        
        //Pivoteo en P
        temp = P(k,:)
        P(k,:) = P(kpivot,:)
        P(kpivot,:) = temp;
        
        //Eliminación de Gauss
        for j = k+1:m
            // L(j,k) = mjk
            L(j,k) = U(j,k)/U(k,k);
            U(j,k:m) = U(j,k:m) - L(j,k)*U(k,k:m);
        end;
    end;
endfunction

// Ejercicio 8

A = [1.012 -2.132 3.104; -2.132 4.096 -7.013; 3.104 -7.013 0.014]

B = [2.1956 4.0231 -2.1732 5.1967; -4.0231 6 0 1.1973; -1 5.2107 1.1111 0; 6.0235 7 0 4.1561]

[Pa,La,Ua] = gaussLU(A);
[Pb,Lb,Ub] = gaussLU(B);
[Las,Uas,Pas] = lu(A)
[Lbs,Ubs,Pbs] = lu(B)

disp("Factorización de A:")
disp("Algoritmo")
disp(La)
disp(Ua)
disp(Pa)
disp("Scilab")
disp(Las)
disp(Uas)
disp(Pas)

// L
//   1.          0.          0.
//  -0.6868557   1.          0.
//   0.3260309  -0.2142473   1.

// U
//   3.104  -7.013       0.014    
//   0.     -0.7209188  -7.003384 
//   0.      0.          1.5989796

// P
//    0.   0.   1.
//    0.   1.   0.
//    1.   0.   0.

// En este caso el algoritmo nos da igual que la implementación
// de scilab

disp("Factorización de B:")
disp("Algoritmo")
disp(Lb)
disp(Ub)
disp(Pb)
disp("Scilab")
disp(Lbs)
disp(Ubs)
disp(Pbs)

// L
//   1.          0.          0.          0.
//  -0.6679007   1.          0.          0.
//   0.3645057   0.1378471   1.          0.
//  -0.1660164   0.596968   -0.5112737   1.

// U
//   6.0235   7.          0.       4.1561   
//   0.       10.675305   0.       3.9731622
//   0.       0.         -2.1732   3.1340889
//   0.       0.          0.      -0.0794924

// P
//   0.   0.   0.   1.
//   0.   1.   0.   0.
//   1.   0.   0.   0.
//   0.   0.   1.   0.

// En este caso el algoritmo nos da igual que la implementación
// de scilab
