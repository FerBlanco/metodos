clc
clear

// Esta función obtiene la solución del sistema de ecuaciones lineales A*x=b, 
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Eliminación Gaussiana con pivoteo parcial.
function [x,a] = gausselimPP(A,b)
    
    [nA,mA] = size(A)
    [nb,mb] = size(b)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    elseif mA<>nb then
        error('gausselim - dimensiones incompatibles entre A y b');
        abort;
    end;
    tic();
    a = [A b]; // Matriz aumentada
    n = nA;
    // Eliminación progresiva
    for k=1:n-1
        
        // Pivoteo
        kpivot = k; amax = abs(a(k,k));  
        for i=k+1:n
            if abs(a(i,k))>amax then
                kpivot = i; amax = abs(a(i,k));
            end;
        end;
        
    temp = a(kpivot,:); a(kpivot,:) = a(k,:); a(k,:) = temp;
    
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik=a(i,k)/a(k,k);
            a(i,k+1:n+1) = a(i,k+1:n+1)-a(k,k+1:n+1)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n) = a(n,n+1)/a(n,n);
    for i = n-1:-1:1
        x(i) = (a(i,n+1)-a(i,i+1:n)*x(i+1:n))/a(i,i)
    end;
    t = toc()
    disp(t,"El tiempo fue de")
endfunction

// Ejemplo de aplicación
/*
A = [6 2 2; 2 0.6667 0.3333; 1 2 -1]
b = [-2 1 0]'

[x,a] = gausselimPP(A,b)

//   2.599928
//  -3.799904
//  -4.99988

//   6.   2.          2.         -2.       
//   0.   1.6666667  -1.3333333   0.3333333
//   0.   0.         -0.33334     1.66666  

A2 = [0 1 2 -1; 1 2 2 0; -1/3 2 0 0; 2 1 1 2]
b2 = [1 3 2 1]'

[x2,a2] = gausselimPP(A2,b2)

A3 = [1 2 -2 1; 4 5 -7 6; 5 25 -15 -3; 6 -12 -6 22]
b3 = [1 2 0 1]'

[x3,a3] = gausselimPP(A3,b3)
*/

// Esta función calcula la inversa de una matriz A
// La función implementa eliminación gausseana.

function [x,a] = inversa(A)
    [nA,mA] = size(A)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    n = nA;
    a = [A eye(n,n)]; 
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(a(k:n,k)));
        pivot = k-1+i;
        temp = a(pivot,:); a(pivot,:) = a(k,:); a(k,:) = temp;
    end;
    
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik = a(i,k)/a(k,k);
            a(i,k+1:2*n) = a(i,k+1:2*n)-a(k,k+1:2*n)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n,:) = a(n,n+1:2*n)/a(n,n);
    
    for i = n-1:-1:1
        x(i,:) = (a(i,n+1:2*n)-a(i,i+1:n)*x(i+1:n,:))/a(i,i)
    end;
endfunction

// Ejemplos de prueba
/*
A = [0 2 1; 2 0 2; 3 3 3]
i = inversa(A)
// i =
//  -1.  -0.5   0.6666667
//   0.  -0.5   0.3333333
//   1.   1.   -0.6666667
*/

// Método de Gauss Seidel
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Gauss Seidel.

function x = gauss(A,b,x0,eps)
    
    n = size(A,1);
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(A(k:n,k)));
        pivot = k-1+i;
        temp = A(pivot,:); A(pivot,:) = A(k,:); A(k,:) = temp;
        temp = b(pivot,:); b(pivot,:) = b(k,:); b(k,:) = temp;
    end;
    
    // Control 
    N = tril(A); // Gauss Seidel usa N = triangular inferior de A
    invN = inversa(N); 
    
    cond = eye(n,n)-invN*A;
    norma = [norm(cond), norm(cond,1), norm(cond,'inf'), norm(cond,'fro')];
    nmin = min(norma);
    
    disp(nmin,"La norma minima fue ");
    
    if nmin >= 1 then
        if max(abs(spec(cond))) >= 1 then
            disp("La solución no converge para todo valor inicial");
            abort;
        end;
    end
    
    tic();
    x = x0;
    xk = x;
    c = 0;
    
    suma = A(1,2:n)*x(2:n);
    x(1) = 1/A(1,1)*(b(1)-suma);
    
    for i = 2:n-1
        suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    
    suma = A(n,1:n-1)*x(1:n-1);
    x(n) = 1/A(n,n)*(b(n)-suma);
    c = c+1;
    
    while abs(norm(x-xk)) > eps
        xk = x;
        
        suma = A(1,2:n)*x(2:n);
        x(1) = 1/A(1,1)*(b(1)-suma);
    
        for i = 2:n-1
            suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        
        suma = A(n,1:n-1)*x(1:n-1);
        x(n) = 1/A(n,n)*(b(n)-suma);
        c = c+1;
    end
    disp(c, "La cantidad de iteraciones fue")
    t = toc()
    disp(t,"El tiempo fue de")
endfunction
   
// EJemplos de aplicación
/*
A = [9 -2 0; -2 4 -1; 0 -1 1]
b = [5 1 0]'

x = gauss(A,b,zeros(3,1),0.00001)
// x = 0.7391295
//     0.8260855
//     0.8260855

Ad = [1 0 -1; 2 2 3; 1 1 1]
bd = [2 4 1]'
x = gauss(Ad,bd,zeros(3,1),0.00001)
// La solución no converge para todo valor inicial
*/

//////////// Ejercicio 4////////////////

function [A,b] = matriz(N)
    A = 8*eye(N,N) + 2*diag(ones(N-1,1),1) + 2*diag(ones(N-1,1),-1)+ diag(ones(N-3,1),3) + diag(ones(N-3,1),-3);
    b = ones(N,1);
endfunction

[Aq,bq] = matriz(500);
[Am,bm] = matriz(1000);

disp("***********Gauss con pivoteo*************")
disp("N = 500")
gausselimPP(Aq,bq);
// tiempo = 2.994599
disp("N = 1000")
gausselimPP(Am,bm);
// tiempo = 22.153106

disp("***********Gauss - Seidel*************")
disp("N = 500")
gauss(Aq,bq,zeros(500,1),10^(-6));
// tiempo = 0.171282
disp("N = 1000")
gauss(Am,bm,zeros(1000,1),10^(-12));
// tiempo = 1.514603

// Se ve que el método iterativo toma un tiempo considerablemente menor
