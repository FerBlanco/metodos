clc
clear

// Esta función calcula la inversa de una matriz A
// La función implementa eliminación gausseana.

function [x,a] = inversa(A)
    [nA,mA] = size(A)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    n = nA;
    a = [A eye(n,n)]; 
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(a(k:n,k)));
        pivot = k-1+i;
        temp = a(pivot,:); a(pivot,:) = a(k,:); a(k,:) = temp;
    end;
    
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik = a(i,k)/a(k,k);
            a(i,k+1:2*n) = a(i,k+1:2*n)-a(k,k+1:2*n)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n,:) = a(n,n+1:2*n)/a(n,n);
    
    for i = n-1:-1:1
        x(i,:) = (a(i,n+1:2*n)-a(i,i+1:n)*x(i+1:n,:))/a(i,i)
    end;
endfunction

// Ejemplos de prueba
/*
A = [0 2 1; 2 0 2; 3 3 3]
i = inversa(A)
// i =
//  -1.  -0.5   0.6666667
//   0.  -0.5   0.3333333
//   1.   1.   -0.6666667
*/

//////// Ejercicio 3 /////////
function A = tri(n)
    uno = -1*ones(1,n-1);
    dos = 2*ones(1,n);
    A = diag(uno,-1)+diag(dos)+diag(uno,1)
endfunction

function x = mgauss(A)
    [n,m] = size(A);
    if n<>m then
        disp("No es cuadrada");
        x = %nan
        return
    end
    N = tril(A);
    x = eye(n,m) - inversa(N)*A
endfunction

A = tri(5)
M = mgauss(A)
// M = 
//   0.   0.5       0.       0.      0.  
//   0.   0.25      0.5      0.      0.  
//   0.   0.125     0.25     0.5     0.  
//   0.   0.0625    0.125    0.25    0.5 
//   0.   0.03125   0.0625   0.125   0.25

function m = mgaussdirecto(n)
    m = zeros(n,n);
    col = (0.5)^[1:n]';
    for k = 1:n-1
        m(k:n,k+1) = col(1:n-k+1);
    end
endfunction


M2 = mgaussdirecto(5)
// M2  = 
//   0.   0.5       0.       0.      0.  
//   0.   0.25      0.5      0.      0.  
//   0.   0.125     0.25     0.5     0.  
//   0.   0.0625    0.125    0.25    0.5 
//   0.   0.03125   0.0625   0.125   0.25
