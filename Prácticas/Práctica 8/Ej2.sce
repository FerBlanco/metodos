clc();clear()
/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction


/////////////// Ejercicio 2 ///////////////
deff("y=a(x)","y=1/x")
deff("y=b(x)","y=x^3")
deff("y=c(x)","y=x*((1+x^3)^(1/2))")
deff("y=d(x)","y=sin(%pi*x)")
deff("y=e(x)","y=x*sin(x)")
deff("y=f(x)","y=(x^2)*(%e^x)")

at = trapecio_c(a,1,3,4)
as = intg(1,3,a)
disp(as, "Comando de Scilab",at,"Trapecio compuesto")
/*
 Trapecio compuesto

   1.1166667

 Comando de Scilab

   1.0986123
*/

bt = trapecio_c(b,0,2,4)
bs = intg(0,2,b)
disp(bs, "Comando de Scilab",bt,"Trapecio compuesto")
/*
 Trapecio compuesto

   4.25

 Comando de Scilab

   4.
*/

ct = trapecio_c(c,0,3,6)
cs = intg(0,3,c)
disp(cs, "Comando de Scilab",ct,"Trapecio compuesto")
/*
 Trapecio compuesto

   14.606357

 Comando de Scilab

   14.357592
*/

dt = trapecio_c(d,0,1,8)
ds = intg(0,1,d)
disp(ds, "Comando de Scilab",dt,"Trapecio compuesto")
/*
 Trapecio compuesto

   0.6284174

 Comando de Scilab

   0.6366198
*/

et = trapecio_c(e,0,2,8)
es = intg(0,2,e)
disp(es, "Comando de Scilab",et,"Trapecio compuesto")
/*

 Trapecio compuesto

   1.7420025

 Comando de Scilab

   1.7415911
*/

ft = trapecio_c(f,0,1,8)
fs = intg(0,1,f)
disp(fs, "Comando de Scilab",ft,"Trapecio compuesto")
/*
 Trapecio compuesto

   0.7288902

 Comando de Scilab

   0.7182818
*/
