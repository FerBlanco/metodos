clc();clear()
/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction

/*
Metodo del Simpson compuesto
Aproxima el una integral por medio de la regla del Simpson compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = simpson_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a) + f(b);
    for i=1:n-1
        if pmodulo(i,2) == 0 then
            s = s+2*f(a+i*h)
        else
            s = s+4*f(a+i*h)
        end
    end
    y = (h/3)*s;
endfunction


//////////////// Ejercicio 4 ///////////////77
deff("y=I(x)","y=(x+1)^(-1)")
It = trapecio_c(I,0,1.5,10)
Is = simpson_c(I,0,1.5,10)
disp(Is,"Simpson compuesto",It,"Trapecio compuesto")
/*
 Trapecio compuesto

   0.9178617

 Simpson compuesto

   0.9163064
*/
