clc();clear()
/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(g,a,b,n)
    h = (b-a)/n;
    s = g(a)/2 + g(b)/2;
    for i=1:n-1
        s = s+g(a+i*h)
    end
    y = h*s;
endfunction

/* 
Aproxima una integral de 2 variables
donde [a,b] es un intervalo y [c(x),d(x)] el otro
tal que c y d sean funciones aplicables en a<=x<=b
y f la funcion a integrar
*/
function t = trapeciodoble_c(f,a,b,c,d,n,m)
    h=(b-a)/n;
    deff("z=fxa(y)","z = f(a,y)")
    deff("z=fxb(y)","z = f(b,y)")
    s = (trapecio_c(fxa,c(a),d(a),m)/2)+(trapecio_c(fxb,c(b),d(b),m)/2);
    for i=1:n-1
        xi = a+i*h;
        deff("z=fxi(y)","z=f(xi,y)");
        s = s+trapecio_c(fxi,c(xi),d(xi),m);
    end
    t = h*s;
endfunction


/*
Metodo del Simpson compuesto
Aproxima el una integral por medio de la regla del Simpson compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = simpson_c(g,a,b,n)
    h = (b-a)/n;
    s = g(a) + g(b);
    for i=1:n-1
        if pmodulo(i,2) == 0 then
            s = s+2*g(a+i*h)
        else
            s = s+4*g(a+i*h)
        end
    end
    y = (h/3)*s;
endfunction

/* 
Metodo Simpson compuesto para 2 variables
Aproxima una integral de 2 variables usando Simpson
donde [a,b] es un intervalo y [c(x),d(x)] el otro
tal que c y d sean funciones aplicables en a<=x<=b
y f la funcion a integrar
*/
function t = simpsondoble_c(f,a,b,c,d,n,m)
    h=(b-a)/n;
    deff("z=fxa(y)","z = f(a,y)")
    deff("z=fxb(y)","z = f(b,y)")
    s = simpson_c(fxa,c(a),d(a),m)+simpson_c(fxb,c(b),d(b),m);
    for i=1:n-1
        xi = a+i*h;
        deff("z=fxi(y)","z=f(xi,y)");
        if pmodulo(i,2) == 0 then
            s = s+2*simpson_c(fxi,c(xi),d(xi),m);
        else
            s = s+4*simpson_c(fxi,c(xi),d(xi),m);
        end
        
        
    end
    t = (h/3)*s;
endfunction

/////////////// Ejercicio 6 /////////////////
deff("z = uno(x,y)","z=1")
deff("y = c(x)","y = -sqrt(2*x-x^2)")
deff("y = d(x)","y = sqrt(2*x-x^2)")

t = trapeciodoble_c(uno,0,2,c,d,100,100)
disp(t)
// 3.1382685

ts = simpsondoble_c(uno,0,2,c,d,100,100)
disp(ts)
// 3.1402926
