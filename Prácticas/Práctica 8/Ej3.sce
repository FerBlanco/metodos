clc();clear()
/*
Metodo del Simpson compuesto
Aproxima el una integral por medio de la regla del Simpson compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = simpson_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a) + f(b);
    for i=1:n-1
        if pmodulo(i,2) == 0 then
            s = s+2*f(a+i*h)
        else
            s = s+4*f(a+i*h)
        end
    end
    y = (h/3)*s;
endfunction


/////////////// Ejercicio 2 ///////////////
deff("y=a(x)","y=1/x")
deff("y=b(x)","y=x^3")
deff("y=c(x)","y=x*((1+x^3)^(1/2))")
deff("y=d(x)","y=sin(%pi*x)")
deff("y=e(x)","y=x*sin(x)")
deff("y=f(x)","y=(x^2)*(%e^x)")

at = simpson_c(a,1,3,4)
as = intg(1,3,a)
disp(as, "Comando de Scilab",at,"Simpson compuesto")
/*
 Simpson compuesto

   1.1

 Comando de Scilab

   1.0986123
*/

bt = simpson_c(b,0,2,4)
bs = intg(0,2,b)
disp(bs, "Comando de Scilab",bt,"Simpson compuesto")
/*
 Simpson compuesto

   4.

 Comando de Scilab

   4.
*/

ct = simpson_c(c,0,3,6)
cs = intg(0,3,c)
disp(cs, "Comando de Scilab",ct,"Simpson compuesto")
/*
 Simpson compuesto

   14.357987

 Comando de Scilab

   14.357592
*/

dt = simpson_c(d,0,1,8)
ds = intg(0,1,d)
disp(ds, "Comando de Scilab",dt,"Simpson compuesto")
/*
 Simpson compuesto

   0.6367055

 Comando de Scilab

   0.6366198
*/

et = simpson_c(e,0,2,8)
es = intg(0,2,e)
disp(es, "Comando de Scilab",et,"Simpson compuesto")
/*

 Simpson compuesto

   1.7415494

 Comando de Scilab

   1.7415911
*/

ft = simpson_c(f,0,1,8)
fs = intg(0,1,f)
disp(fs, "Comando de Scilab",ft,"Simpson compuesto")
/*
 Simpson compuesto

   0.7183215

 Comando de Scilab

   0.7182818
*/
