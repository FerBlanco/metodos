clc();clear()
/*
Metodo del trapecio
Aproxima el una integral por medio de la regla del trapecio
a y b son los extremos de integracion 
f es la funcion a integrar
*/
function y = trapecio(f,a,b)
    h = (b-a);
    y = (h/2)*(f(a)+f(b))
endfunction

/*
Metodo del Simpson 
Aproxima el una integral por medio de la regla del Simpson
a y b son los extremos de integracion 
f es la funcion a integrar
*/
function y = simpson(f,a,b)
    h = (b-a)/2;
    y = (h/3)*(f(a)+4*f(a+h)+f(b));
endfunction


////////// Ejercicio 1 //////////////
at = trapecio(log,1,2)
as = simpson(log,1,2)
ar = intg(1,2,log)
disp(ar,"Comando Scilab",as,"Metodo de Simpson",at,"Metodo de trapecio")
/*
 Metodo de trapecio
   0.3465736

 Metodo de Simpson
   0.3858346

 Comando Scilab
   0.3862944
*/

deff("y=f(x)","y=x^(1/3)")
bt = trapecio(f,0,0.1)
bs = simpson(f,0,0.1)
br = intg(0,0.1,f)
disp(br,"Comando Scilab",bs,"Metodo de Simpson",bt,"Metodo de trapecio")
/*
Metodo de trapecio
   0.0232079

 Metodo de Simpson
   0.0322962

 Comando Scilab
   0.0348119
*/

deff("y=s(x)","y=sin(x)^2")
ct = trapecio(s,0,%pi/3)
cs = simpson(s,0,%pi/3)
cr = intg(0,%pi/3,s)
disp(cr,"Comando Scilab",cs,"Metodo de Simpson",ct,"Metodo de trapecio")
/*
Metodo de trapecio
   0.3926991

 Metodo de Simpson
   0.3054326

 Comando Scilab
   0.3070924
*/