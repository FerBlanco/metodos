clc();clear()

/* 
Aproxima una integral de 2 variables
donde [a,b] es un intervalo y [c,d] el otro
y f la funcion a integrar
*/

function y = trapeciodoble(f,a,b,c,d)
    h = (b-a)*(d-c)/4;
    y = h*(f(c,a)+f(c,b)+f(d,a)+f(d,b));
endfunction

//////// Ejercicio 4 //////////////
deff("z=f(x,y)","z=sin(x+y)")
t = trapeciodoble(f,0,2,0,1)
disp(t)
// 0.9459442
