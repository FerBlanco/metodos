// suponer que la ley de la función f es un string
// esta escrita usando la variable x
// a es el punto alrededor de donde se va a evaluar f
// n es el grado del polinomio de taylor
// v es el punto donde se quiere evaluar f

function t = taylor(f,a,n,v)
    c = coeff_taylor(f,n,0.001,a);
    p = poly(c,'x','c');
    t = horner(p,(v-a))
endfunction

// Función que calcula los coeficientes del polinomio de Taylor
// f, n y v son iguales a la de la función taylor
// h es el paso de derivación

function a = coeff_taylor(f,n,h,v)
a = 0:n;
deff("y=DF0(x)","y="+f);
    a(1) = DF0(v);
    for i = 1:n
        deff("y=DF"+string(i)+"(x)","y=numderivative(DF"+string(i-1)+",x,"+string(h)+",4)");
   	deff("y=DFn(x)","y=DF"+string(i)+"(x)");     
        a(i+1) = DFn(v)/factorial(i);
    end
endfunction


// ##############################################################################
// ##############################################################################
// ##############################################################################
// ##############################################################################


// f es una función polinomica
// a es el punto alrededor de donde se va a evaluar f
// n es el grado del polinomio de taylor
// x es el punto donde se quiere evaluar f
function p = taylorp(f,a,n,x)
    p=horner(f,a);
    for i = 1:n
        f = derivat(f);
        p=p+(1/factorial(i))*horner(f,a)*((x-a)^i)
    end
endfunction

function p = taylor1(f,h,x)
    [J,H] = numderivative(f,x);
    p = f(x+h)-J*h-(h/2)*H*h;
endfunction


