// suponer que la ley de la función f es un string
// esta escrita usando la variable x
// v es un valor donde se evaluará la derivada
// n es el orden de derivada
// h es el paso de derivación

function d = derivarI(f,v,n,h)
    deff("y=DF0(x)","y="+f);
    if n == 0 then d = DF0(v);
       else
    for i = 1:(n-1)
        deff("y=DF"+string(i)+"(x)","y=(DF"+string(i-1)+"(x+"+string(h)+")-DF"+string(i-1)+"(x))/"+string(h));
    end
    deff("y=DFn(x)","y=(DF"+string(n-1)+"(x+"+string(h)+")-DF"+string(n-1)+"(x))/"+string(h));
    d = DFn(v);    
    end
endfunction

// derivarI("x^3",2,1,0.00001)
// ans  =
// 
//   12.00006

// usando numderivative
// numderivative(f,v,h,orden)
// suponer que la ley de la función f es un string
// esta escrita usando la variable x
// v es un valor donde se evaluará la derivada
// n es el orden de derivada
// h es el paso de derivación

function d = derivarNum(f,v,n,h)
    deff("y=DF0(x)","y="+f);
    if n == 0 then d = DF0(v);
       else
    for i = 1:(n-1)
        deff("y=DF"+string(i)+"(x)","y=numderivative(DF"+string(i-1)+",x,"+string(h)+",4)");
    end
    deff("y=DFn(x)","y=numderivative(DF"+string(n-1)+",x,"+string(h)+",4)");
    d = DFn(v);    
    end
endfunction

// derivarNum("x^3",2,1,0.00001)
//  ans  = 12.


// suponer que la ley de la función f es un string
// esta escrita usando la variable x
// a es el punto alrededor de donde se va a evaluar f
// n es el grado del polinomio de taylor
// v es el punto donde se quiere evaluar f
function p = taylor1(f,a,n,v)
    deff("y=DF0(x)","y="+f);
    p = DF0(a);
    for i = 1:n
        p = p + (1/factorial(i))*derivarNum(f,a,i,0.001)*((v-a)^i);
    end
endfunction


// taylor("x^3",0,5,-2)
// ans  =
//
//  -8.

// taylor("exp(x)",0,5,2)
//  ans  =
// 
//    7.2629759


// calcular todas las derivadas en un vector
