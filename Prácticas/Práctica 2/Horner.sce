function b = Horner(p,x0,n)
    b = coeff(p,n);
    for i = 1:n
        b = coeff(p,n-i) + x0*b
    end
endfunction

function b = HornerG(p,x0,n)
    b(1) = coeff(p,n);
    b(2) = b(1)*(x0^(n-1));
    for i = 1:n-1
        b(1) = coeff(p,n-i) + x0*b(1);
        b(2) = b(2) + b(1)*(x0^(n-(i+1)));
    end
    b(1) = coeff(p,0)+x0*b(1)
endfunction
