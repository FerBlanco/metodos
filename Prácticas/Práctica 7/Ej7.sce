//Aproximacion de minimos cuadrados
/*x,y vectores tales que(x(i),y(i))son los puntos dados para realizar 
la aproximación, n es el grado del polinomio, P es el polinomio de aproximación
de mínimos cuadrados, y ERR el error cometido
*/
function[P,ERR] = minimoscuadrados(x,y,n)
    m=length(x);
    A=zeros(m,n+1); //Acontiene los valores de las funciones
                    //Φ0(xi),Φ1(xi),...,Φn(xi)en la fila i-ésima, 
                    //por lo que tiene n+1columnas

// Definimos la matriz A 
    for i = 1:m
        for j = 1:n+1
            A(i,j) = x(i)^(j-1);
        end
    end 
    
// Calculamos ATA y ATb 
    Amin = A'*A;
    bmin = A'*y';
    
// Calculamos la solución 'a'
    a = inv(Amin)*bmin;
    
// Definimos el polinomio de aproximación
    P = poly(a,"s",["coeff"]);
    
// Calculamos el error de aproximación
    ERR = norm(A*a-y');
endfunction

////// Ejemplos de aplicacion ///////
// Para probarlos descomentar
/*
x = [0 0.12 0.37 0.46 0.53 1 1.02]
y = [2 1.7 1.5 1.23 0.76 1.36 2.07]

[p,e] = minimoscuadrados(x,y,3)
intervalo = linspace(-1,1.5)
plot(intervalo,horner(p,intervalo),'r')
scatter(x,y)
disp(e,"El error fue de:")
*/


//////////////////// Ejercicio 7 ///////////////////
x = [0 0.15 0.31 0.5 0.6 0.75]
y = [1 1.004 1.031 1.117 1.223 1.422]

[p1,e1] = minimoscuadrados(x,y,1);
disp(e1,"error",p1,"polinomio grado 1")
// p1 = 0.929514 +0.5281021s
// e1 = 0.1567356

[p2,e2] = minimoscuadrados(x,y,2);
disp(e2,"error",p2,"polinomio grado 2")
// p2 = 1.011341 -0.3256988s +1.1473303s²
// e2 = 0.0307449

[p3,e3] = minimoscuadrados(x,y,3);
disp(e3,"error",p3,"polinomio grado 3")
// p3 = 1.0004398-0.001541s-0.0115057s²+1.0210226s³ 
// e3 = 0.0105469


/* En base a los errores podemos decir que el polinomio de grado 3
 es la mejor aproximación
 */
