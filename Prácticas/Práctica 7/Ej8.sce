clc();clear();
//Aproximacion de minimos cuadrados
/*x,y vectores tales que(x(i),y(i))son los puntos dados para realizar 
la aproximación, n es el grado del polinomio, P es el polinomio de aproximación
de mínimos cuadrados, y ERR el error cometido
*/
function[P,ERR] = minimoscuadrados(x,y,n)
    m=length(x);
    A=zeros(m,n+1); //Acontiene los valores de las funciones
                    //Φ0(xi),Φ1(xi),...,Φn(xi)en la fila i-ésima, 
                    //por lo que tiene n+1columnas

// Definimos la matriz A 
    for i = 1:m
        for j = 1:n+1
            A(i,j) = x(i)^(j-1);
        end
    end 
    
// Calculamos ATA y ATb 
    Amin = A'*A;
    bmin = A'*y';
    
// Calculamos la solución 'a'
    a = inv(Amin)*bmin;
    
// Definimos el polinomio de aproximación
    P = poly(a,"s",["coeff"]);
    
// Calculamos el error de aproximación
    ERR = norm(A*a-y');
endfunction


////// Ejemplos de aplicacion ///////
// Para probarlos descomentar
/*
x = [0 0.12 0.37 0.46 0.53 1 1.02]
y = [2 1.7 1.5 1.23 0.76 1.36 2.07]

[p,e] = minimoscuadrados(x,y,3)
intervalo = linspace(-1,1.5)
plot(intervalo,horner(p,intervalo),'r')
scatter(x,y)
disp(e,"El error fue de:")
*/


//////////////// Ejercicio 8 ///////////////////

x = [4 4.2 4.5 4.7 5.1 5.5 5.9 6.3 6.8 7.1]
y = [102.56 113.18 130.11 142.05 167.53 195.14 224.87 256.73 299.5 326.72]

[p1,e1] = minimoscuadrados(x,y,1);
disp(e1,"error",p1,"polinomio grado 1")
// p1 = 0.929514 +0.5281021s
// e1 = 0.1567356

[p2,e2] = minimoscuadrados(x,y,2);
disp(e2,"error",p2,"polinomio grado 2")
// p2 = 1.011341 -0.3256988s +1.1473303s²
// e2 = 0.0307449

[p3,e3] = minimoscuadrados(x,y,3);
disp(e3,"error",p3,"polinomio grado 3")
// p3 = 1.0004398-0.001541s-0.0115057s²+1.0210226s³ 
// e3 = 0.0105469

intervalo = linspace(3.5,7.5)
plot(intervalo,horner(p1,intervalo))
plot(intervalo,horner(p2,intervalo),'g')
plot(intervalo,horner(p3,intervalo),'r')
scatter(x,y)
