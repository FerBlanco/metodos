clear(); clc();

/*
Metodo de Lagrange
Esta función calcula el polinomio interpolante 
usando el metodo de Lagrange
Dado una funcion f y un vector de puntos x
devuelve el polinomio p que interpola a f
*/
function p = i_lagrange(y,x)
    [n,m] = size(x);
    p = 0;
    for k = 1 : m
        p = p + Lk(k,x)*y(k);
    end
endfunction

/*
Lk arma la funcion usada en el metodo de Lagrange
*/
function p = Lk(k,x)
    [n,m] = size(x);
    r = [x(1:k-1) x(k+1:m)];
    p1 = poly(r,"x","roots");
    e = horner(p1,x(k));
    p = p1 / e;
endfunction

//// Ejemplos de prueba ////
/*
deff("y=f(x)","y=log(x)/log(10)");
x = [0.1,0.2,0.6,1]
y = f(x);
x2 = [0.2,0.6]
y2 = f(x2)
p = i_lagrange(y,x)
p2 = i_lagrange(y2,x2)
rango = 0.01:0.01:2
plot(rango,f(rango),'r');
plot(rango,horner(p,rango),'g');
plot(rango,horner(p2,rango),'b');
*/

/*
Metodo de diferencias divididas de Newton
Esta función calcula el polinomio interpolante 
usando el metodo de diferencias divididas de Newton
v, w vectores de valores donde (v(i),w(i)) son los datos 
dados para la interpolación.
P es el polinomio de interpolación por diferencias divididas 
correspondiente
*/
function P = i_newton(v,w)
    n = length(v);
    P = w(1);
    for k = 2:n do
        pr = poly(v(1:k-1),"x","roots");
        D = difdivk(v(1:k),w(1:k));
        P = P + pr*D;
    end 
    endfunction


/* 
Diferencias divididas de orden k
x, y vectores de valores donde (x(i),y(i)) son los datos dados 
para la interpolación, y D es la diferencia dividida de orden k 
correspondiente
*/
function D=difdivk(x,y)
    k = length(x);
    if k==2 then
        D = (y(2)-y(1))/(x(2)-x(1));
    else
        D = (difdivk(x(2:k),y(2:k))-difdivk(x(1:k-1),y(1:k-1)))/(x(k)-x(1));
    end 
endfunction

//// Ejemplos de prueba ////
/*
deff("y=f(x)","y=log(x)/log(10)");
x = [0.1,0.2,0.6,1]
y = f(x);
x2 = [0.2,0.6]
y2 = f(x2)
p = i_newton(x,y)
p2 = i_newton(x2,y2)
rango = 0.01:0.01:2
plot(rango,f(rango),'r');
plot(rango,horner(p,rango),'g');
plot(rango,horner(p2,rango),'b');
*/



//////////////////// Ejercicio 1 ///////////////////////////

// Datos para interpolacion cubica
xc = [0 0.2 0.4 0.6]
yc = [1 1.2214 1.4918 1.8221]

// Datos para interpolacion lineal
xl = [0 0.6]
yl = [1 1.8221]

pcl = i_lagrange(yc,xc)
pcn = i_newton(xc,yc)
pll = i_lagrange(yl,xl)
pln = i_newton(xl,yl)

rll = horner(pll,1/3)
//   1.4567222
rln = horner(pln,1/3)
//   1.4567222
rcl = horner(pcl,1/3)
//   1.3955494
rcn = horner(pcn,1/3)
//   1.3955494
disp(rll,"Resultado con interpolacion lineal por Lagrange")
disp(rln,"Resultado con interpolacion lineal por diferencia divididas de Newton")
disp(rcl,"Resultado con interpolacion cubica por Lagrange")
disp(rcn,"Resultado con interpolacion cubica por diferencia divididas de Newton")
