clc();clear();
/*
Metodo de diferencias divididas de Newton
Esta función calcula el polinomio interpolante 
usando el metodo de diferencias divididas de Newton
v, w vectores de valores donde (v(i),w(i)) son los datos 
dados para la interpolación.
P es el polinomio de interpolación por diferencias divididas 
correspondiente
*/
function P = i_newton(v,w)
    n = length(v);
    P = w(1);
    for k = 2:n do
        pr = poly(v(1:k-1),"x","roots");
        D = difdivk(v(1:k),w(1:k));
        P = P + pr*D;
    end 
    endfunction


/* 
Diferencias divididas de orden k
x, y vectores de valores donde (x(i),y(i)) son los datos dados 
para la interpolación, y D es la diferencia dividida de orden k 
correspondiente
*/
function D=difdivk(x,y)
    k = length(x);
    if k==2 then
        D = (y(2)-y(1))/(x(2)-x(1));
    else
        D = (difdivk(x(2:k),y(2:k))-difdivk(x(1:k-1),y(1:k-1)))/(x(k)-x(1));
    end 
endfunction

//// Ejemplos de prueba ////
/*
deff("y=f(x)","y=log(x)/log(10)");
x = [0.1,0.2,0.6,1]
y = f(x);
x2 = [0.2,0.6]
y2 = f(x2)
p = i_newton(x,y)
p2 = i_newton(x2,y2)
rango = 0.01:0.01:2
plot(rango,f(rango),'r');
plot(rango,horner(p,rango),'g');
plot(rango,horner(p2,rango),'b');
*/


/////////////////// Ejercicio 4 ////////////////////
x = [2 2.1 2.2 2.3 2.4 2.5]
y = [0.2239 0.1666 0.1104 0.0555 0.0025 -0.0484]

p = i_newton(x,y);

v1 = horner(p,2.15);
v2 = horner(p,2.35);

disp(v2,"J0(2.35) = ",v1,"J0(2.15) =")
// J0(2.15) =
//    0.1383688
// J0(2.35) = 
//    0.0287313
