clc();clear()
//////// Interpolacion Polinomica //////////////

/*
Metodo de Lagrange
Esta función calcula el polinomio interpolante 
usando el metodo de Lagrange
Dado una funcion f y un vector de puntos x
devuelve el polinomio p que interpola a f
*/
function p = i_lagrange(y,x)
    [n,m] = size(x);
    p = 0;
    for k = 1 : m
        p = p + Lk(k,x)*y(k);
    end
endfunction

/*
Lk arma la funcion usada en el metodo de Lagrange
*/
function p = Lk(k,x)
    [n,m] = size(x);
    r = [x(1:k-1) x(k+1:m)];
    p1 = poly(r,"x","roots");
    e = horner(p1,x(k));
    p = p1 / e;
endfunction

//// Ejemplos de prueba ////
/*
deff("y=f(x)","y=log(x)/log(10)");
x = [0.1,0.2,0.6,1]
y = f(x);
x2 = [0.2,0.6]
y2 = f(x2)
p = i_lagrange(y,x)
p2 = i_lagrange(y2,x2)
rango = 0.01:0.01:2
plot(rango,f(rango),'r');
plot(rango,horner(p,rango),'g');
plot(rango,horner(p2,rango),'b');
*/


/*
Chev calcula n nodos de Chebyshev en un intervalo [a,b]
*/
function r = chev(n,a,b)
    for i = 0:(n-1)
        x = cos((%pi/(2*n))*(1+2*i));
        r(i+1) = ((b+a)-x*(b-a))/2;
        if 2*i<n then
            r(n-i) = -r(i+1)
        end
    end
    
endfunction


///////////////// Ejercicio 11 //////////////////7
x = chev(4,0,%pi/2)

// Veamos graficamente el error[0,%pi/2]
y = cos(x);
p= i_lagrange(y',x') // Polinomio de interpolacion
i = linspace(0,%pi/2) 
plot(i,cos(i)-horner(p,i))

