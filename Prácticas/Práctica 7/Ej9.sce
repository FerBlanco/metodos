clc();clear();
//////// Interpolacion Polinomica //////////////

/*
Metodo de Lagrange
Esta función calcula el polinomio interpolante 
usando el metodo de Lagrange
Dado una funcion f y un vector de puntos x
devuelve el polinomio p que interpola a f
*/
function p = i_lagrange(y,x)
    [n,m] = size(x);
    p = 0;
    for k = 1 : m
        p = p + Lk(k,x)*y(k);
    end
endfunction

/*
Lk arma la funcion usada en el metodo de Lagrange
*/
function p = Lk(k,x)
    [n,m] = size(x);
    r = [x(1:k-1) x(k+1:m)];
    p1 = poly(r,"x","roots");
    e = horner(p1,x(k));
    p = p1 / e;
endfunction

//// Ejemplos de prueba ////
/*
deff("y=f(x)","y=log(x)/log(10)");
x = [0.1,0.2,0.6,1]
y = f(x);
x2 = [0.2,0.6]
y2 = f(x2)
p = i_lagrange(y,x)
p2 = i_lagrange(y2,x2)
rango = 0.01:0.01:2
plot(rango,f(rango),'r');
plot(rango,horner(p,rango),'g');
plot(rango,horner(p2,rango),'b');
*/

/////////// Ejercicio 9 //////////////

function errorinterpolacion(f,n,a,b)
    h = (b-a)/n;
    x = a:h:b; // nodos de interpolacion
    for i = 1:(n+1)
        y(i) = f(x(i)); //f(xi)
    end
    p = i_lagrange(y,x); // polinomio de interpolacion
    
    // Calculo de error
    intervalo = linspace(a,b)
    deff("y=e(x)","y=abs(f(x)-horner(p,x))")
    //plot(intervalo,e,'g')
    plot(intervalo,f)
    plot(intervalo,horner(p,intervalo),'r')
endfunction

deff("y=f(x)","y = 1/(1+x^2)");
for n = [2 4 6 7 9 10 14]
    clf()
    //errorinterpolacion(f,n,-5,5)
    x = 0:1/n:1
    [0 x]
    p = poly(x,"x","roots");
    it = linspace(0,1)
    plot(it,horner(p,it))
    title("phi(x) orden "+ string(n))
    input("next");
end

/* 
Se puede ver que al aumentar el n el valor maximo del error
aumenta, lo cual es conocido como el Fenómeno de Runge.
*/

