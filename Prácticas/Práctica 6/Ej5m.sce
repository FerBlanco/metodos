// Esta funcion utiliza el metodo de la potencia para calcular el 
// autovalor dominante.
// Donde A es una matriz cuadrada, z0 el vector inicial z0, 
// eps es tolerancia al  error y maxiter el numero max de iteraciones.
// Los valores de retorno corresponden a: 
//    - valor:  autovalor dominante
//    - zn: el vector asociado a el autovalor

function [valor,zn] = mpotencia(A,z0, eps, maxiter)
    
    valor = 0
    iter = 1 
    
    w = A*z0
    zn = w / norm(w, %inf) //diferentes normas, diferentes convergencias
    
    [m,j] = max(abs(w))
    valor = m/zn(j)
    
    while (iter<maxiter && norm(z0-zn, %inf)> eps)
        z0 = zn
        
        w = A*z0
        zn = w / norm(w, %inf)
        
        [m,j] = max(abs(w))
        valor = m/zn(j)
        
        iter = iter + 1
    end
    disp(iter,"Iteraciones")
endfunction

function [valor,zn, comp] = iterMpotencia(A,z0)
    lambda = max(spec(A))
    
    valor = 0
    iter = 1 
    
    w = A*z0
    zn = w / norm(w, %inf) //diferentes normas, diferentes convergencias
    
    [m,j] = max(abs(w))
    valor = m/zn(j)
     
    er = abs(lambda-valor)
    comp(iter) = er
    while (er > %eps)
        z0 = zn
        
        w = A*z0
        zn = w / norm(w, %inf)
        
        [m,j] = max(abs(w))
        valor = m/zn(j)
        
        er = abs(lambda-valor)
        comp(iter) = er
        iter = iter + 1
    end
    disp(iter,"Iteraciones necesarias:")
endfunction

  z = [1 1 1 1]'
  
  A1 = [6 4 4 1
        4 6 1 4
        4 1 6 4
        1 4 4 6]
        
  A2 = [12  1  3  4
         1 -3  1  5
         3  1  6 -2
         4  5 -2 -1]
         
  disp("Item a:")
  disp(A1, "Matriz A1:")
  [l,v] = mpotencia(A1,z, 0.000001, 1000)
  disp(l,  "Autovalor aproximado:")
  disp(v,  "Vector asociado:")
  
  disp(A2, "Matriz A2:")
  [l2,v2] = mpotencia(A2,z, 0.000001, 1000)
  disp(l2, "Autovalor aproximado:")
  disp(v2, "Vector asociado:")

  disp("Item b:")
  disp(A1, "Matriz A1:")
  [l,v,comp] = iterMpotencia(A1,z)
  disp(l,  "Autovalor aproximado:")
  disp(v,  "Vector asociado:")
  subplot(121)
  plot(comp,'o-')
  xlabel("Iteracion")
  ylabel("Error")
  
  disp(A2, "Matriz A2:")
//  [l2,v2,comp2] = iterMpotencia(A2,z)
//  disp(l2, "Autovalor aproximado:")
//  disp(v2, "Vector asociado:")
//  subplot(122)
//  plot(comp2,'o-')
//  xlabel("Iteracion")
//  ylabel("Error")
