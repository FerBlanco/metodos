///////////// Ejercicio 1 ////////////////

a = [1 0 0;-1 0 1;-1 -1 2]
b = [1 0 0;-0.1 0 0.1;-0.1 -0.1 2]
c = [1 0 0;-0.25 0 0.25;-0.25 -0.25 2]
d = [4 -1 0;-1 4 -1;-1 -1 4]
e = [3 2 1;2 3 0;1 0 3]
f = [4.75 2.25 -0.25;2.25 4.75 1.25;-0.25 1.25 4.75]

//disp(spec(a))

//   1.  
//   1.  
//   1.  

//disp(spec(b))

//   1.9949874  
//   0.0050126  
//   1. 

//disp(spec(c))

//   1.9682458  
//   0.0317542  
//   1.  

//disp(spec(d))

//   4.618034  
//   2.381966  
//   5. 

//disp(spec(e))

//   0.763932
//   3.
//   5.236068

//disp(spec(f))

//   2.0646374
//   4.9616991
//   7.2236635
