clc()
clear()

// Esta función realiza el método de la potencia
// Dada una matriz A, un vector inicial z0
// una tolerancia eps y una cantidad de iteraciones iter
// calcula el maximo autovalor y su autovector asociado
// mediante el método de la potencia
function [valor,zn] = m_potencia(A,z0,eps,maxIter)
    // Primera iteración
    w = A*z0;
    zn = w/norm(w,%inf);
    iter = 1;
    
    while norm(zn-z0) > eps & iter < maxIter
        z0 = zn;
        w = A*z0;
        zn = w/norm(w,%inf);
        iter = iter+1;
    end
    
    // Autovalor
    [m,k] = max(w);
    valor = m/z0(k);
    
    disp(iter,"Cantidad de iteraciones");
endfunction

// Ejemplos de prueba
//A = [1 -1 0;-2 4 -2; 0 -1 3]
//[lambda,v] = m_potencia(A,[1 1 2]',0.0001,150)

// Cantidad de iteraciones 13.
// v  = 
//   0.231286
//  -1.
//   0.4304549
//
// lambda  = 
//   5.3227707

//A = [1 0 0;-1 0 -1;-1 -1 2]
//[lambda,v] = m_potencia(A,[1 1 1]',0.0001,1000)

// Cantidad de iteraciones 14.
// v  = 
//   0.0000299
//  -0.4142434
//   1.
//
// lambda  = 
//   2.4142136

//////////////////////// Ejercicio 5 ///////////////////////////

A1 = [6 4 4 1;4 6 1 4; 4 1 6 4;1 4 4 6]
A2 = [12 1 3 4;1 -3 1 5;3 1 6 -2;4 5 -2 -1]
inicial = [1 0 1 2]'
eps = 0.0001
maxiter = 150

[l1,v1] = m_potencia(A1,inicial,eps,150)
[l2,v2] = m_potencia(A2,inicial,eps,150)

disp(v1, "Autovector asociado",l1,"Autovalor maximo","Matriz A1")

// Matriz A1
//
// Autovalor maximo
//
//   14.999746
//
// Autovector asociado
//
//   0.9999831
//   0.9999831
//   1.
//   1.

disp(v2, "Autovector asociado",l2,"Autovalor maximo","Matriz A2")

// Matriz A2
//
// Autovalor maximo
//
//   14.20114
//
// Autovector asociado
//
//   1.
//   0.1558791
//   0.3183476
//   0.2725027


// Item b


// Esta función realiza el método de la potencia
// Dada una matriz A, un vector inicial z0
// una tolerancia eps y una cantidad de iteraciones iter
// calcula el maximo autovalor y su autovector asociado
// mediante el método de la potencia comparando con el dado por scilab
function [valor,zn] = m_potenciab(A,z0)
    lambda = max(spec(A));
    // Primera iteración
    w = A*z0;
    zn = w/norm(w,%inf);
    
    [m,k] = max(w);
    valor = m/z0(k);
    
    iter = 1;
    er = abs(lambda-valor)
    while er > %eps//*20
        z0 = zn;
        w = A*z0;
        zn = w/norm(w,%inf);
        [m,k] = max(w);
        valor = m/z0(k);
        er = abs(lambda-valor);
        iter = iter+1;
    end
    
    disp(iter,"Cantidad de iteraciones");
endfunction

m_potenciab(A1,inicial)
//m_potenciab(A2,[1 1 1 1]')
