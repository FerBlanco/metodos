// Blanco Fernando
clc
clear

// Esta función calcula la inversa de una matriz A
// La función implementa eliminación gausseana.

function [x,a] = inversa(A)
    [nA,mA] = size(A)

    if nA<>mA then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    n = nA;
    a = [A eye(n,n)]; 
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(a(k:n,k)));
        pivot = k-1+i;
        temp = a(pivot,:); a(pivot,:) = a(k,:); a(k,:) = temp;
    end;
    
    // Eliminación progresiva
    for k=1:n-1
        for i=k+1:n
            // Ei = Ei - mik*Ek
            mik = a(i,k)/a(k,k);
            a(i,k+1:2*n) = a(i,k+1:2*n)-a(k,k+1:2*n)*mik;
            a(i,k)=0
        end;
    end;
    
    // Sustitución regresiva
    x(n,:) = a(n,n+1:2*n)/a(n,n);
    
    for i = n-1:-1:1
        x(i,:) = (a(i,n+1:2*n)-a(i,i+1:n)*x(i+1:n,:))/a(i,i)
    end;
endfunction

// Ejemplos de prueba
disp("Ejemplo de prueba de inversa")
A = [0 2 1; 2 0 2; 3 3 3]
i = inversa(A)
disp(i)
// i =
//  -1.  -0.5   0.6666667
//   0.  -0.5   0.3333333
//   1.   1.   -0.6666667

// Método de Gauss Seidel
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Gauss Seidel.

function [x,h] = gauss(A,b,x0,maxiter)
    n = size(A,1);
    
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(A(k:n,k)));
        pivot = k-1+i;
        temp = A(pivot,:); A(pivot,:) = A(k,:); A(k,:) = temp;
        temp = b(pivot,:); b(pivot,:) = b(k,:); b(k,:) = temp;
    end;
    
    // Control 
    N = tril(A); // Gauss Seidel usa N = triangular inferior de A
    invN = inversa(N); 
    
    cond = eye(n,n)-invN*A;
    
    // Chequeo de norma
    norma = [norm(cond), norm(cond,1), norm(cond,'inf'), norm(cond,'fro')];
    nmin = min(norma);
    disp(nmin,"La norma minima fue ");
    
    if nmin >= 1 then
        // Chequeo de espectro
        if max(abs(spec(cond))) >= 1 then
            disp("La solución no converge para todo valor inicial");
            x=%nan;
            return;
        end;
    end
    
    h(:,1) = x0;
    // Primera iteración
    x = x0;
    xk = x;
    c = 0;
    
    suma = A(1,2:n)*x(2:n);
    x(1) = 1/A(1,1)*(b(1)-suma);
    
    for i = 2:n-1
        suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    
    suma = A(n,1:n-1)*x(1:n-1);
    x(n) = 1/A(n,n)*(b(n)-suma);
    c = c+1;
    h(:,2) = x;
    
    while c < maxiter
        xk = x;
        
        suma = A(1,2:n)*x(2:n);
        x(1) = 1/A(1,1)*(b(1)-suma);
    
        for i = 2:n-1
            suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        
        suma = A(n,1:n-1)*x(1:n-1);
        x(n) = 1/A(n,n)*(b(n)-suma);
        c = c+1;
        h(:,c+1) = x;
    end
    disp(c, "La cantidad de iteraciones fue")
endfunction
   
// Ejemplo de pruebas
disp("Ejemplos de prueba de Gauss Seidel")
A = [9 -2 0; -2 4 -1; 0 -1 1]
b = [5 1 0]'

x = gauss(A,b,zeros(3,1),13)
disp(x)
// x = 0.7391295
//     0.8260855
//     0.8260855

Ad = [1 0 -1; 2 2 3; 1 1 1]
bd = [2 4 1]'
x = gauss(Ad,bd,zeros(3,1),10)
// La solución no converge para todo valor inicial


// Método de Jacobi
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A y el vector b.
// La función implementa el método de Jacobi.

function [x,h] = jacobi(A,b,x0,maxiter)
    n = size(A,1);
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(A(k:n,k)));
        pivot = k-1+i;
        temp = A(pivot,:); A(pivot,:) = A(k,:); A(k,:) = temp;
        temp = b(pivot,:); b(pivot,:) = b(k,:); b(k,:) = temp;
    end;
    
    // Control 
    N = diag(diag(A)); // Jacobi usa N = diagonal(A)
    
    for i = 1:n
        invN(i) = 1/N(i,i); // diagonal de N^(-1)
    end
    invN = diag(invN);
    
    cond = eye(n,n)-invN*A;
    norma = [norm(cond), norm(cond,1), norm(cond,'inf'), norm(cond,'fro')];
    
    // Chequeo la norma
    nmin = min(norma);
    disp(nmin,"La norma minima fue ");
    
    if nmin >= 1 then
        // Chequeo el radio espectral
        if max(abs(spec(cond))) >= 1 then
            disp("La solución no converge para todo valor inicial");
            x = %nan;
            return;
        end;
    end
    h(:,1)=x0;
    xk = x0;
    c = 0;
    
    // Primera iteración
    suma = A(1,2:n)*xk(2:n)
    x(1) = 1/A(1,1)*(b(1)-suma)
    
    for i = 2:n-1
        suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n)
        x(i) = 1/A(i,i)*(b(i)-suma);
    end;
    
    suma = A(n,1:n-1)*xk(1:n-1)
    x(n) = 1/A(n,n)*(b(n)-suma)
    c = c+1;
    h(:,2) = x;

    while c < maxiter
        xk = x;
        suma = A(1,2:n)*xk(2:n)
        x(1) = 1/A(1,1)*(b(1)-suma)
    
        for i = 2:n-1
            suma = A(i,1:i-1)*xk(1:i-1)+A(i,i+1:n)*xk(i+1:n)
            x(i) = 1/A(i,i)*(b(i)-suma);
        end;
        
        suma = A(n,1:n-1)*xk(1:n-1)
        x(n) = 1/A(n,n)*(b(n)-suma)
        c = c+1;
        h(:,c+1)=x;
    end
    disp(c,"La cantidad de iteraciones fue ")
endfunction
   
// Ejemplos de prueba
disp("Ejemplos de prueba de Jacobi")
A = [9 -2 0; -2 4 -1; 0 -1 1]
b = [5 1 0]'

x = jacobi(A,b,zeros(3,1),24)
disp(x)
// x = 0.7391287
//     0.8260829
//     0.8260791

Ad = [1 0 -1; 2 2 3; 1 1 1]
bd = [2 4 1]'
x = jacobi(Ad,bd,zeros(3,1),10)
// La solución no converge para todo valor inicial

// Método de sobrerelajación
// Método iterativo para resolver sistemas de ecuaciones lineales

// Esta función resuelve un sistema de la forma Ax = b
// dada la matriz de coeficientes A, el vector b y un factor w.
// La función implementa el método de sobrerelajación.

function [x,h] = sor(A,b,x0,maxiter,w)
    n = size(A,1);
    
    // Pivoteo
    for k = 1:n-1
        [v,i] = max(abs(A(k:n,k)));
        pivot = k-1+i;
        temp = A(pivot,:); A(pivot,:) = A(k,:); A(k,:) = temp;
        temp = b(pivot,:); b(pivot,:) = b(k,:); b(k,:) = temp;
    end;
    
    // Control 
    L = tril(A,-1);
    D = diag(diag(A));
    U = triu(A,1);
    T = inversa(D+w*L)*((1-w)*D-w*U);
    
    if max(abs(spec(T))) >= 1 then
        disp("La solución no converge para todo valor inicial");
        x = %nan;
        return;
    end;
    
    h(:,1)=x0;
    x = x0;
    xk = x;
    c = 0;
    
    suma = A(1,2:n)*x(2:n);
    x(1) = w/A(1,1)*(b(1)-suma)+(1-w)*x(1);
    
    for i = 2:n-1
        suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
        x(i) = w/A(i,i)*(b(i)-suma)+(1-w)*x(i);
    end;
    
    suma = A(n,1:n-1)*x(1:n-1);
    x(n) = w/A(n,n)*(b(n)-suma)+(1-w)*x(n);
    c = c+1;
    h(:,2) = x;
    
    while c < maxiter
        xk = x;
        
        suma = A(1,2:n)*x(2:n);
        x(1) = w/A(1,1)*(b(1)-suma)+(1-w)*x(1);
    
        for i = 2:n-1
            suma = A(i,1:i-1)*x(1:i-1)+A(i,i+1:n)*x(i+1:n);
            x(i) = w/A(i,i)*(b(i)-suma)+(1-w)*x(i);
        end;
        
        suma = A(n,1:n-1)*x(1:n-1);
        x(n) = w/A(n,n)*(b(n)-suma)+(1-w)*x(n);
        c = c+1;
        h(:,c+1) = x;
    end
    disp(c, "La cantidad de iteraciones fue")
endfunction
   
// Ejemplos de aplicacion
disp("Ejemplo de prueba SOR")
A = [9 -2 0; -2 4 -1; 0 -1 1]
b = [5 1 0]'

x1 = sor(A,b,zeros(3,1),13,1) // Gauss - Seidel 
disp(x1)
// x = 0.7391295
//     0.8260855
//     0.8260855

Ad = [1 0 -1; 2 2 3; 1 1 1]
bd = [2 4 1]'
x = sor(Ad,bd,zeros(3,1),10,-1)
// La solución no converge para todo valor inicial

A2 = [3 2 0; 2 3 -1; 3 -1 3]
b2 = [5 2 5]'
x2 = sor(A2,b2,ones(3,1),39,1.25)
disp(x2)
// x2  = 
//   2.9999899
//  -1.9999881
//  -1.9999869


//////////////// Ejercicio 4 /////////////////////

// Nota: Los algoritmos fueron modificados para que la condición de parada
// sea la cantidad de iteraciones en lugar del error de aproximación para 
// realizar este ejercicio, además se agregó que devuelva un historial de 
// todos los xk que fueron calculando

disp("Ejercicio 4")
A =[4 -2 0; -2 5 1; 0 1 2]
b = [0 2 -1]'
x0 = [0 0 0]'
w = 1.09

[xj,hj] = jacobi(A,b,x0,20)
disp(xj)
// xj =
//   0.3571398
//   0.7142815
//  -0.8571398

[xg,hg] = gauss(A,b,x0,20)
disp(xg)
// xg = 
//   0.3571429
//   0.7142857
//  -0.8571429

[xs,hs] = sor(A,b,x0,20,w)
disp(xs)
// xs = 
//   0.3571429
//   0.7142857
//  -0.8571429

function y=err(A,x,b)
    m = A*x-b;
    y = norm(m,'inf');
endfunction

deff("y=fj(x)","y=norm(A*hj(:,x)-b,%inf)")
deff("y=fg(x)","y=norm(A*hg(:,x)-b,%inf)")
deff("y=fs(x)","y=norm(A*hs(:,x)-b,%inf)")
i = 1:20
plot(i,fj,"o.",'color','red')
plot(i,fg,"o.",'color','blue')
plot(i,fs,"o.",'color','green')
title("Jacobi = Rojo  Gauss = Azul  Sor = Verde")

// en la gráfica puede verse que el método de Jacobi es el que tarda más en 
// achicar el error mientras que el sor es el más veloz, esto por darle w > 1
// El método de Gauss-Seidel se mantiene entre ambos ya que es más veloz que 
// Jacobi y es equivalente a un sor con w = 1 por lo que al tener menor w 
// tarda más
