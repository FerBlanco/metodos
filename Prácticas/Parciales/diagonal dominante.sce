// Blanco Fernando
clc
clear

function B = diag_dom(A)
    [n,m] = size(A)
    
    if n<>m then
        error('gausselim - La matriz A debe ser cuadrada');
        abort;
    end;
    
    B = A;
    b = 1 // b = 1 matriz diagonal dominante
    for i = 1:n-1
        suma = sum(abs(B(i,1:i-1)))+sum(abs(B(i,i+1:n)))
        if abs(B(i,i)) < suma
            b = 0; // b = 0 matriz no diagonal dominante
            // Pivoteo
            [v,p] = max(abs(B(i:n,i:m))); // Máximo de la submatriz
            row = i-1+p(1);
            col = i-1+p(2);
            temp = B(row,:); B(row,:) = B(i,:); B(i,:) = temp; // Intercambio de fila
            temp = B(:,col); B(:,col) = B(:,i); B(:,i) = temp; // Intercambio de columnas
            end;
    end;
    
    if b == 1 then
        disp("La matriz es diagonalmente dominante");
    else
        disp("La matriz no era diagonalmente dominante, se realizo pivoteo tota")
    end
endfunction

A = [5 2 1; 0 -3 2; -4 1 6]
diag_dom(A)
disp(ans)
// ans = 
//   5.   2.   1.
//   0.  -3.   2.
//  -4.   1.   6.
 
A2 = [1 2 -2 1; 4 5 -7 6; 5 25 -15 -3; 6 -12 -6 22]
diag_dom(A2)
disp(ans)
// ans = 
//   25.  -3.   -15.   5.
//  -12.   22.  -6.    6.
//   5.    6.   -7.    4.
//   2.    1.   -2.    1.


A3 = [2 5 1; 2 0 -3 ; -4 1 6]
diag_dom(A3)
disp(ans)
// ans = 
//   6.   1.  -4.
//   1.   5.   2.
//  -3.   0.   2.

// Después del proceso de pivoteo total no siempre queda una matriz 
// diagonalmente dominante, si en la fila i la suma de los |A(i,j)| con 
// j distinto que k es mayor que |A(i,k)| para 1 <= k <= n, la matriz nunca
// va a ser diagonal dominante ya que las operaciones de intercambio de fila 
// y/o columnas no cambian el valor de la suma. 
