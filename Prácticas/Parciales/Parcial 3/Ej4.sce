// Blanco Fernando
clc()
clear()

// Esta función realiza el método de la potencia
// Dada una matriz A, un vector inicial z0
// una tolerancia eps y una cantidad de iteraciones iter
// calcula el maximo autovalor y su autovector asociado
// mediante el método de la potencia
function [valor,zn] = m_potencia(A,z0,eps,maxIter)
    // Primera iteración
    w = A*z0;
    zn = w/norm(w,%inf);
    iter = 1;
    
    while norm(zn-z0) > eps & iter < maxIter
        z0 = zn;
        w = A*z0;
        zn = w/norm(w,%inf);
        iter = iter+1;
    end
    
    // Autovalor
    [m,k] = max(w);
    valor = m/z0(k);
    
    disp(iter,"Cantidad de iteraciones");
endfunction

// Ejemplos de prueba
/*
A = [1 -1 0;-2 4 -2; 0 -1 3]
[lambda,v] = m_potencia(A,[1 1 2]',0.0001,150)
disp(v,"Autovector asociado: ",lambda, "Autovalor maximo: ")
*/
/*
 Cantidad de iteraciones

   13.

 Autovalor maximo: 

   5.3227707

 Autovector asociado: 

   0.231286
  -1.
   0.4304549
*/
/*
A = [1 0 0;-1 0 -1;-1 -1 2]
[lambda,v] = m_potencia(A,[1 1 1]',0.0001,1000)
disp(v,"Autovector asociado: ",lambda, "Autovalor maximo: ")
*/
/*
 Cantidad de iteraciones

   14.

 Autovalor maximo: 

   2.4142136

 Autovector asociado: 

   0.0000299
  -0.4142434
   1.
*/



//////////////////////// ejercicio 4 ////////////////////

A = [0.4 0.2 0.4; 0.3 0.7 0; 0.3 0.1 0.6]
[lambda,v] = m_potencia(A,[0.3 0.2 0]',0.0001,1000)
disp(v,"Autovector asociado: ",lambda, "Autovalor maximo: ")
/*
Cantidad de iteraciones

   19.

 Autovalor maximo: 

   0.9999708

 Autovector asociado: 

   0.9999415
   1.
   0.9999269
*/
