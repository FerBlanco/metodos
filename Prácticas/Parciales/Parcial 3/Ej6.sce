// Blanco Fernando
clc();clear()
/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction

///////////////// Ejercicio 6 //////////////////
deff("y=I(x)","y=%e^(2*x)")
t = trapecio_c(I,0,1,50)
disp(t,"Metodo de trapecio")
// t = 3.194954
it = intg(0,1,I)
disp(it,"Comando intg")
// it = 3.194528
disp(abs(it-t),"Error")
// err = 0.0004259
