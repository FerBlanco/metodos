// Blanco Fernando
clc();clear()
/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction


/////////////// Ejercicio 2 ///////////////
deff("y=b(x)","y=x^3")
deff("y=c(x)","y=x*((1+x^3)^(1/2))")


bt = trapecio_c(b,0,2,4)
bs = intg(0,2,b)
disp(bs, "Comando de Scilab",bt,"Trapecio compuesto")
disp(abs(bs-bt),"Error")
/*
 Trapecio compuesto

   4.25

 Comando de Scilab

   4.
 Error

   0.25
*/

ct = trapecio_c(c,0,3,6)
cs = intg(0,3,c)
disp(cs, "Comando de Scilab",ct,"Trapecio compuesto")
disp(abs(cs-ct),"Error")
/*
 Trapecio compuesto

   14.606357

 Comando de Scilab

   14.357592

   Error

   0.2487648
*/
