clc 
clear
// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    //x = x0;
    x = g(x0)
    while abs(x-x0)>e && iter < maxIter
        x0 = x;
        x = g(x0);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
//deff("y=f(x)","y=(%e^(2*x-6)+cos(x))");
//r = m_puntofijo(f,0.4,0.0001,30)
// ans  = 0.7456289

// Método de Newton
// f es una función derivable
// x0 es un punto 
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = m_newton(f,x0,e,maxIter)
    iter = 0;
    x = x0-f(x0)/numderivative(f,x0);
    while (abs(x-x0)>e) && (iter < maxIter)
        x0 = x;
        x=x0-f(x0)/numderivative(f,x0);
        iter=iter+1;
    end;
    if (abs(x-x0)>e) 
        then x = %nan
    end
endfunction

// Prueba
//deff("y=p(x)","y=x^2*cos(x)")
//r = m_newton(p,1.5,0.0001,50)
//disp(r)
// r  = 1.5707963


/////////////// Ejercicio 7 /////////////////////

// h = 4 m profundidad de una ola
// T = 5 speriodo de una ola
// l longitud de onda
// w = 2pi/T una pulsación
// g = 9.8 m/s^2 aceleración de la gravedad
// d = 2pi/l número de onda
// w² = gdtanh(hd) dispersión

h = 4;
T = 5;
gr = 9.8;

//a) calcular d con punto fijo


deff("y=f(x)","y=(gr*T^2*tanh(h*(2*%pi)/x))/(2*%pi)");
l0 = m_puntofijo(f,2*%pi,0.1,1000); 
disp(l0)
// 27.955173


//b) calcular l con newton
deff("y=f2(x)","y=(gr*T^2*tanh(h*(2*%pi)/x))/(2*%pi) - x")
l = m_newton(f2,l0,0.0001,1000)
disp(l)
// l = 27.928588

