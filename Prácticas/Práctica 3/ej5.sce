clc
clear 
// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    //x = x0;
    x = g(x0)
    while abs(x-x0)>e && iter < maxIter
        x0 = x;
        x = g(x0);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
//deff("y=f(x)","y=x^2");
//r = m_puntofijo(f,0.4,0.0001,10)
//disp(r)
// ans  = 0.0000004


//////////////////// Ejercicio 5 ////////////////////////

deff("y=g(x)","y=2^(x-1)");
r = m_puntofijo(g,0,0.00001,30)
disp(r)
// r = 0.9999841
