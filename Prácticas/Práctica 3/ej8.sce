clc
clear
// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    //x = x0;
    x = g(x0)
    while abs(x-x0)>e && iter < maxIter
        x0 = x;
        x = g(x0);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
//deff("y=f(x)","y=(%e^(2*x-6)+cos(x))");
//r = m_puntofijo(f,0.4,0.0001,30)
// ans  = 0.7456289

                   ////// Ejercicio 8 ///////
format(10)
// Graficamos la ecuación para ver donde estan las raices
deff("y=f(x)","y=%e^x-3*x");
plot(-1:0.01:2,f);
xgrid();
// los puntos fijos se encuentran cerca de 0.6 y 1.5

// a)
deff("y=g1(x)","y=(%e^x)/3");
x1 = m_puntofijo(g1,0,0.00000001,40)
disp("a) x = ")
disp(x1)

// x1 = 0.6190613 

// b)
deff("y=g2(x)","y=(%e^x-x)/2");
x2 = m_puntofijo(g2,0,0.00000001,30)
disp("b) x =")
disp(x2)

// x2 = 0.6190613

//c)
deff("y=g3(x)","y=log(3*x)");
x3 = m_puntofijo(g3,1.1,0.00000001,50)
disp("c) x =")
disp(x3)

// x3 = 1.5121345

// d)
deff("y=g4(x)","y=%e^x-2*x");
x4 = m_puntofijo(g4,0,0.000000001,30)
disp("d) x =")
disp(x4)

// x4 = 0.6190613

