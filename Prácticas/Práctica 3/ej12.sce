clc
clear
// Método de Newton
// f es una matriz derivable
// x0 es un vector de puntos iniciales
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = mm_newton(f,x0,e,maxIter)
    iter = 0;
    x = x0-inv(numderivative(f,x0))*f(x0);
    while or((abs(x-x0)>e)) && (iter < maxIter)
        x0 = x;
        x=x0-inv(numderivative(f,x0))*f(x0);
        iter=iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
function y=f(x)
 f1 = sqrt(x(1)+x(2))-1;
 f2 = x(1)^2+x(2)^2-sin(x(1)*x(2))-1;
 y=[f1;f2];
endfunction

disp("Casos de prueba para el método de Newton")
r = mm_newton(f,[0.5;0.7],0.0001,10)
disp(r)
// ans  = -7.871D-10
//         1.

// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    //x = x0;
    x = g(x0)
    while abs(x-x0)>e && iter < maxIter
        x0 = x;
        x = g(x0);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
deff("y=f(x)","y=(%e^(2*x-6)+cos(x))");
plot([-3:0.1:5],f)

disp("Casos de prueba para el método de punto fijo")
r = m_puntofijo(f,0.4,0.0001,30)
disp(r)
// ans  = 0.7456289

/////////// Ejercicio 12 ////////////////////

// a)

function y = f(k)
    l1 = k(1)*%e^(k(2))+k(3)-10;
    l2 = k(1)*%e^(k(2)*2)+k(3)*2-12;
    l3 = k(1)*%e^(k(2)*3)+k(3)*2-15;
    y = [l1;l2;l3]
endfunction

k = mm_newton(f,[3;1;7],0.0000001,10)
// k  =
//   8.4600049
//   0.2095982
//  -0.43273
disp("a)")
disp("Los valores de k son : ");
disp(k)

//b)
disp("b)")

function y=f2(r)
    y = sqrt(500/(k(1)*%e^(k(2)*r)+k(3)*r));
endfunction


// Utilizando punto fijo encontramos al radio minimo
r = m_puntofijo(f2,5,0.0000001,30);
// r  =  4.8480746

disp("El radio minimo para sostener 500 libras sumergiendose 1 pie = ");
disp(r)


