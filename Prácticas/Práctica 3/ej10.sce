clc
clear
// Método de Newton
// f es una matriz derivable
// x0 es un vector de puntos iniciales
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = mm_newton(f,x0,e,maxIter)
    iter = 0;
    x = x0-inv(numderivative(f,x0))*f(x0);
    while or((abs(x-x0)>e)) && (iter < maxIter)
        x0 = x;
        x=x0-inv(numderivative(f,x0))*f(x0);
        iter=iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
//function y=f(x)
// f1 = sqrt(x(1)+x(2))-1;
// f2 = x(1)^2+x(2)^2-sin(x(1)*x(2))-1;
// y=[f1;f2];
//endfunction

//r = mm_newton(f,[0.5;0.7],0.0001,10)
//disp(r)
// ans  = -7.871D-10
//         1.

/////////////Ejercicio 10///////////////

function y=f(x)
    f1 = x(1)^2+x(1)*x(2)^3-9;
    f2 = 3+x(1)^2*x(2)-4+x(2)^3;
    y=[f1;f2]
endfunction

a = [1.2;2.5];
b = [-2;2.5];
c = [-1.2;-2.5];
d = [2;-2.5];

ra = mm_newton(f,a,0.0001,20);
rb = mm_newton(f,b,0.0001,25);
rc = mm_newton(f,c,0.0001,20);
rd = mm_newton(f,d,0.0001,15);

disp("ra =")
disp(ra)
disp("rb =")
disp(rb)
disp("rc =")
disp(rc)
disp("rd =")
disp(rd)

// ra =
//   2.9993161
//   0.1110097

// rb =
//  -3.0006822
//   0.1109091

// rc =
//  -3.0006822
//   0.1109091

// rd =
//  -3.0006822
//   0.1109091
