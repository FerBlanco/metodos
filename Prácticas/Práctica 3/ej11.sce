clc
clear
// Ejercicio 11

function y=f(x)
    y = 2*x(1)+3*x(2)^2+%e^(2*x(1)^2+x(2)^2);
endfunction

// Se desea ver que f'= 0 por lo que modifico el método de Newton 
// para que dada una función f calcule la raíz de su derivada


// Método de Newton
// f es una matriz derivable
// x0 es un vector de puntos iniciales
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = mm_newton(f,x0,e,maxIter)
    
    iter = 0;
    
    // J matriz Jacobiana, derivada primera
    // H matriz Hessiana, derivada segunda
    [J, H] = numderivative(f, x0, [], [], "blockmat")
    x = x0-inv(H)*J';
    
    while norm(x-x0)>e && (iter < maxIter)
        x0=x;
        [J, H] = numderivative(f, x0, [], [], "blockmat")
        x = x0-inv(H)*J';
        iter=iter+1;
    end
    
    if iter == maxIter 
        then x = %nan
    end
endfunction
eps = 10^(-12)
x = mm_newton(f,[1;1],eps,1000);
disp(x)
//x  = 
//  -0.3765446
//  -0.0000029

[J,H] = numderivative(f,x,[],[],"blockmat");
disp(H)
//H = 
//   8.3238128   0.      
//   0.          667.4888


// La matriz hessiana tiene forma de triángular superior, luego sus 
// autovalores son los elementos de la diagonal los cuales son positivos
// por lo tanto es definida positiva.
// Luego el punto x sacado anteriormente es un minimo de f

