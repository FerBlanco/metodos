clc
clear 
// Método de Newton
// f es una matriz derivable
// x0 es un vector de puntos iniciales
// e es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = mm_newton(f,x0,e,maxIter)
    iter = 0;
    x = x0-inv(numderivative(f,x0))*f(x0);
    while or((abs(x-x0)>e)) && (iter < maxIter)
        x0 = x;
        x=x0-inv(numderivative(f,x0))*f(x0);
        iter=iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction

// Prueba
//function y=f(x)
// f1 = sqrt(x(1)+x(2))-1;
// f2 = x(1)^2+x(2)^2-sin(x(1)*x(2))-1;
// y=[f1;f2];
//endfunction

//r = mm_newton(f,[0.5;0.7],0.0001,10)
//disp(r)
// ans  = -7.871D-10
//         1.

    //////////////// Ejercicio 9 //////////////////
    
function y = f(x)
    f1 = 1 + x(1)^2 - x(2)^2 + %e^(x(1))*cos(x(2));
    f2 = 2*x(1)*x(2)+ %e^(x(1))*sin(x(2));
    y=[f1;f2]
endfunction

x0 = [-1;4];

r=mm_newton(f,x0,0.0001,5);
printf("El resultado es x = %f, y = %f\n",r(1),r(2));
// x = -0.293163, y = 1.172660


