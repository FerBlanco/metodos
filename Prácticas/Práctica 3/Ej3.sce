// Método de la secante (Algoritmo de la resolución)
// f es un función
// a y b son puntos 
// eps es la tolerancia al error
// maxIter es la cantidad máxima de iteraciones
function x = m_secante(f,a,b,eps,maxIter)
    iter = 1
    fb = f(b)
    fa = f(a)
    actual = b - fb*(b-a)/(fb-fa);
    anterior = b;
    while (abs(f(actual)) >= eps) && (iter < maxIter)
        // actual = xn
        // anterior = x(n-1)
        // siguiente = x(n+1)
        fant = f(anterior);
        fact = f(actual);
        siguiente = actual - fact * (actual-anterior)/(fact-fant);
        anterior = actual;
        actual = siguiente;
        iter = iter + 1;
    end
    if iter == maxIter
        x = %nan
    else x = actual
    end
endfunction

// Ejercicio 3

deff("y=f(x)", "y=(x^2)/4 - sin(x)");
x = m_secante(f,1,3,0.00001,10);
// x = 1.9337575

