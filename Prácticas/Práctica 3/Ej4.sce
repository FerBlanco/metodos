// Método de punto fijo
// g es una función
// x0 es un punto inicial
// e es la tolerancía al error
// maxIter es la cantidad máxima de iteraciones
// Este método puede calcular raices de funciones f eligiendo una adecuada g
function x = m_puntofijo(g,x0,e,maxIter)
    iter = 0;
    x = x0;
    while abs(g(x)-x)>e
        x = g(x);
        iter = iter+1;
    end
    if iter == maxIter 
        then x = %nan
    end
endfunction


// Ejercicio 4
// Lo que nos pide es aplicar el método de punto fijo 
// con g(x) = cos(x)
// Veamos que cos(x) tiene punto fijo:
// Para ello aplicaremos el teorema 2 
// Notemos que para cualquier x0 vale que 
// -1 <= x1=cos(x0) <= 1 -> -1 <=cos(x1)<=1
// por lo que para todo xi con i>0 cos(xi) satisface esa condición
// Luego cos'(x) = -sin(x) y |-sin(x)| < 1 para todo x tq -1<=x<=1
// por lo que sup|cos'(x)| < 1
// Luego por teorema 2 podemos asegurar que existe un único punto fijo
// y para cualquier valor inicial entre -1 y 1 la iteración converge
// como vimos que para cualquier x0 resulta -1<=x1=cos(x0)<=1 por lo que 
// la iteración converge.

m_puntofijo(cos,5,0.000000001,250)
// ans  = 0.7390851


