// Método de la bisección
// f es una función 
// a y b son dos puntos tal que f es continua en [a,b] y f(a)f(b)<=0
// e es la tolerancía al error

function c = m_biseccion(f,a,b,e)
    if f(a)*f(b)>0 
        then abort
    else c = (a+b)/2;
         while (b-c>e)
             if f(b)*f(c) <= 0 
                 then a = c;
             else b = c;
             end
         c = (a+b)/2;
     end
     end
endfunction

// Ejercicio 2
// seteamos la tolerancia en 10^-2
e = 0.01;

// Notemos que toda ecuación a = b puede 
// escribirse como a-b=0
// Luego buscar las raices de las ecuaciones 
// es lo mismo que buscar las raices de una función f = a-b

// a) sin x = x^2 / 2 
deff("y=f(x)","y=sin(x)-x^2/2");
x1 = m_biseccion(f,-1,1,e)
x2 = m_biseccion(f,1,2,e)
//x1  =  0.0078125
//x2  =  1.3984375

// b) e^-x = x^4
deff("y=f(x)","y=%e^(-x)-x^4");
x1 = m_biseccion(f,-2,0,e)
x2 = m_biseccion(f,0,2,e) 
//x1  = -1.4296875
//x2  = 0.8203125

// c) log x = x-1
deff("y=f(x)","y=log10(x)-x+1");
x1 = m_biseccion(f,0.1,0.5,e)
x2 = m_biseccion(f,0.5,2,e) 
//x1  = 0.13125
//x2  = 0.9980469
