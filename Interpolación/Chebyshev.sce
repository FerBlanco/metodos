clc();clear()

// Polinomio de Chebyshev
/* 
chebyshev genera un polinomio de Chebyshev de grado n
*/
function t = chebyshev(n)
    t0 = poly(1,"x",'c')
    if n == 0 then
        t = t0
        return
    end
    
    t1 = poly([0 1],"x",'c')
    t=t1
    for i = 2:n
        t1 = t;
        c = coeff(t);
        t = poly([0 2*c],"x",'c')-t0
        t0=t1
    end
endfunction

/*
Chev calcula n nodos de Chebyshev en un intervalo [a,b]
*/
function r = chev(n,a,b)
    for i = 0:(n-1)
        x = cos((%pi/(2*n))*(1+2*i));
        r(i+1) = ((b+a)-x*(b-a))/2;
        if 2*i<n then
            r(n-i) = -r(i+1)
        end
    end
    
endfunction


/////////// Ejemplo de prueba ////////
t0 = chebyshev(0)
// t0 = 1
t1 = chebyshev(1)
// t1 = x
t2 = chebyshev(2)
// t2 = -1+2x²
t5 = chebyshev(5)
// t5 = 5x - 20x³ + 16x⁵
