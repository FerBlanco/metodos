clc();clear();
//Aproximacion de minimos cuadrados
/*x,y vectores tales que(x(i),y(i))son los puntos dados para realizar 
la aproximación, n es el grado del polinomio, P es el polinomio de aproximación
de mínimos cuadrados, y ERR el error cometido
*/
function[P,ERR] = minimoscuadrados(x,y,n)
    m=length(x);
    A=zeros(m,n+1); //Acontiene los valores de las funciones
                    //Φ0(xi),Φ1(xi),...,Φn(xi)en la fila i-ésima, 
                    //por lo que tiene n+1columnas

// Definimos la matriz A 
    for i = 1:m
        for j = 1:n+1
            A(i,j) = x(i)^(j-1);
        end
    end 
    
// Calculamos ATA y ATb 
    Amin = A'*A;
    bmin = A'*y';
    
// Calculamos la solución 'a'
    a = inv(Amin)*bmin;
    
// Definimos el polinomio de aproximación
    P = poly(a,"s",["coeff"]);
    
// Calculamos el error de aproximación
    ERR = norm(A*a-y');
endfunction


////// Ejemplos de aplicacion ///////
// Para probarlos descomentar
/*
x = [0 0.12 0.37 0.46 0.53 1 1.02]
y = [2 1.7 1.5 1.23 0.76 1.36 2.07]

[p,e] = minimoscuadrados(x,y,3)
intervalo = linspace(-1,1.5)
plot(intervalo,horner(p,intervalo),'r')
scatter(x,y)
disp(e,"El error fue de:")
*/
