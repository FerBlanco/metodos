clc();clear()
/*
Metodo del trapecio
Aproxima el una integral por medio de la regla del trapecio
a y b son los extremos de integracion 
f es la funcion a integrar
*/
function y = trapecio(f,a,b)
    h = (b-a);
    y = (h/2)*(f(a)+f(b))
endfunction

/*
Metodo del trapecio compuesto
Aproxima el una integral por medio de la regla del trapecio compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = trapecio_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a)/2 + f(b)/2;
    for i=1:n-1
        s = s+f(a+i*h)
    end
    y = h*s;
endfunction

////// Ejemplos ///////
deff("y=f(x)","y=x^3")
t = trapecio(f,1,2)
// -0.5186986
t5 = trapecio_c(f,1,2,5)
// -3.7659395

/* 
Aproxima una integral de 2 variables
donde [a,b] es un intervalo y [c(x),d(x)] el otro
tal que c y d sean funciones aplicables en a<=x<=b
y f la funcion a integrar
*/
function y = trapeciodoble_c(f,a,b,c,d,n,m)
    h=(b-a)/n
    deff("z=fxa(y)","y=f(a,y)")
    deff("z=fxb(y)","y=f(b,y)")
    s = trapecio_c(fxa,c(a),d(a),m)/2+trapecio_c(fxb,c(b),d(b),m)/2;
    for i=1:n-1
        xi = a+i*h;
        deff("z=fxi(y)","y=f(xi,y)");
        s = s+trapecio_c(fxi,c(xi),d(xi),m);
    end
    y = h*s;
endfunction


/* 
Aproxima una integral de 2 variables
donde [a,b] es un intervalo y [c,d] el otro
y f la funcion a integrar
*/

function y = trapeciodoble(f,a,b,c,d)
    h = (b-a)*(d-c)/4;
    y = h*(f(c,a)+f(c,b)+f(d,a)+f(d,b));
endfunction
