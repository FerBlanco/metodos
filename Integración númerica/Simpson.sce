clc();clear()
/*
Metodo del Simpson 
Aproxima el una integral por medio de la regla del Simpson
a y b son los extremos de integracion 
f es la funcion a integrar
*/
function y = simpson(f,a,b)
    h = (b-a)/2;
    y = (h/3)*(f(a)+4*f(a+h)+f(b));
endfunction

/*
Metodo del Simpson compuesto
Aproxima el una integral por medio de la regla del Simpson compuesto
a y b son los extremos de integracion 
f es la funcion a integrar
n el numero de intervalo
*/
function y = simpsonp_c(f,a,b,n)
    h = (b-a)/n;
    s = f(a) + f(b);
    for i=1:n-1
        if pmodulo(i,2) == 0 then
            s = s+2*f(a+i*h)
        else
            s = s+4*f(a+i*h)
        end
    end
    y = (h/3)*s;
endfunction
